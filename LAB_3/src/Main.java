import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;

public class Main extends Application {

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    @Override
    public void start(Stage stage) {

        File[] file = new File[1];
        ImageView imageView = new ImageView();
        MenuBar menuBar = new MenuBar();
        final FileChooser fileChooser = new FileChooser();
        setExtFilters(fileChooser);
        Window.windowOpen(stage, imageView, menuBar);

        Menu menuFile = new Menu("File");
        MenuItem open = new MenuItem("Open");
        MenuItem save = new MenuItem("Save");
        MenuItem close = new MenuItem("Close");

        menuFile.getItems().addAll(open, save, close, new SeparatorMenuItem());
        menuBar.getMenus().addAll(menuFile);

        Menu menuRGB = new Menu("RGB");
        MenuItem rgb = new MenuItem("RGB to gray");
        menuRGB.getItems().addAll(rgb);
        menuBar.getMenus().addAll(menuRGB);

        Menu menuAff = new Menu("Affine transformations");
        MenuItem moving = new MenuItem("Moving image");
        MenuItem scaling = new MenuItem("Scaling image");
        MenuItem shift = new MenuItem("Shift image");
        MenuItem rotation = new MenuItem("Rotation of image");
        menuAff.getItems().addAll(moving, scaling, shift, rotation);
        menuBar.getMenus().addAll(menuAff);

        Menu menuChan = new Menu("Channels RGB");
        MenuItem redChannel = new MenuItem("Red");
        MenuItem greenChannel = new MenuItem("Green");
        MenuItem blueChannel = new MenuItem("Blue");
        menuChan.getItems().addAll(redChannel, greenChannel, blueChannel);
        menuBar.getMenus().addAll(menuChan);

        open.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(final ActionEvent e) {
                        file[0] = fileChooser.showOpenDialog(stage);
                        try {
                            BufferedImage images = ImageIO.read(file[0]);
                            Image image = SwingFXUtils.toFXImage(images, null);
                            imageView.setImage(image);
                        } catch (Exception ex) {
                            imageView.setImage(null);
                            Exceptions.checkOpen();
                        }
                    }
                });

        save.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        File file = fileChooser.showSaveDialog(stage);
                        if (file != null) {
                            try {
                                ImageIO.write(SwingFXUtils.fromFXImage(imageView.getImage(),
                                        null), "png", file);
                            } catch (Exception e) {
                                Exceptions.checkOpen();
                            }
                        }
                    }
                });

        close.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        imageView.setImage(null);
                        file[0] = null;
                    }
                });

        rgb.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        BufferedImage image;
                        try {
                            image = ImageIO.read(file[0]);
                            double width = image.getWidth();
                            double height = image.getHeight();
                            for (int i = 0; i < height; i++) {
                                for (int j = 0; j < width; j++) {
                                    Color c = new Color(image.getRGB(j, i));
                                    int red = (int) (c.getRed() * 0.299);
                                    int green = (int) (c.getGreen() * 0.587);
                                    int blue = (int) (c.getBlue() * 0.114);
                                    Color newColor = new Color(red + green + blue, red + green + blue, red + green + blue);
                                    image.setRGB(j, i, newColor.getRGB());
                                }
                            }
                            Image images = SwingFXUtils.toFXImage(image, null);
                            imageView.setImage(images);
                        } catch (Exception e) {
                            Exceptions.checkOpen();
                        }
                    }
                });

        moving.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
                            Stage stage = new Stage();
                            stage.setTitle("Enter Moving");
                            Group root = new Group();
                            Scene scene = new Scene(root, 300, 150);
                            Button btn = new Button();
                            btn.setLayoutX(130);
                            btn.setLayoutY(100);
                            btn.setText("OK");
                            Text text1 = new Text();
                            text1.setText("X:");
                            text1.setLayoutX(10);
                            text1.setLayoutY(30);
                            Text text2 = new Text();
                            text2.setText("Y:");
                            text2.setLayoutX(10);
                            text2.setLayoutY(70);
                            TextField input1 = new TextField();
                            input1.setLayoutX(70);
                            input1.setLayoutY(10);
                            TextField input2 = new TextField();
                            input2.setLayoutX(70);
                            input2.setLayoutY(50);
                            root.getChildren().addAll(btn, input1, input2, text1, text2);
                            stage.setScene(scene);
                            Image ico = new Image("mask.png");
                            stage.getIcons().add(ico);
                            stage.show();
                            btn.setOnAction(new EventHandler<ActionEvent>() {
                                public void handle(ActionEvent event) {
                                    try {
                                        int movingX = Integer.parseInt(input1.getText());
                                        int movingY = Integer.parseInt(input2.getText());
                                        int movX = movingX;
                                        if (movX <= 0) {
                                            movX = 0;
                                        }
                                        int movY = movingY;
                                        if (movY >= 0) {
                                            movY = 0;
                                        }
                                        Mat M = new Mat(2, 3, CvType.CV_32FC1);
                                        M.put(0, 0,
                                                1, 0, movX,
                                                0, 1, -movY
                                        );
                                        Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2BGRA);
                                        Mat img2 = new Mat();
                                        Imgproc.warpAffine(img, img2, M,
                                                new Size(img.width() + Math.abs(movingX), img.height() + Math.abs(movingY)),
                                                Imgproc.INTER_LINEAR, Core.BORDER_CONSTANT,
                                                new Scalar(0, 0, 0, 255));
                                        Image im = Transform.MatToImageFX(img2);
                                        imageView.setImage(im);
                                    } catch (NumberFormatException e) {
                                        Exceptions.checkEnterNumber();
                                    } catch (CvException e) {
                                        Exceptions.checkMany();
                                    }
                                    stage.close();
                                }
                            });
                        } catch (NullPointerException a) {
                            Exceptions.checkOpen();
                        }
                    }
                });

        scaling.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
                            Stage stage = new Stage();
                            stage.setTitle("Enter Scaling");
                            Group root = new Group();
                            Scene scene = new Scene(root, 300, 100);
                            Button btn = new Button();
                            btn.setLayoutX(130);
                            btn.setLayoutY(50);
                            btn.setText("OK");
                            TextField input = new TextField();
                            input.setLayoutX(60);
                            input.setLayoutY(10);
                            root.getChildren().addAll(btn, input);
                            stage.setScene(scene);
                            Image ico = new Image("mask.png");
                            stage.getIcons().add(ico);
                            stage.show();
                            btn.setOnAction(new EventHandler<ActionEvent>() {
                                public void handle(ActionEvent event) {
                                    try {
                                        double scaling = Double.parseDouble(input.getText());
                                        if (scaling > 0 && scaling <= 20) {
                                            Mat M2 = new Mat(2, 3, CvType.CV_32FC1);
                                            M2.put(0, 0,
                                                    scaling, 0, 0,
                                                    0, scaling, 0
                                            );
                                            Mat img2 = new Mat();
                                            Imgproc.warpAffine(img, img2, M2,
                                                    new Size(img.width() * scaling, img.height() * scaling),
                                                    Imgproc.INTER_CUBIC, Core.BORDER_TRANSPARENT,
                                                    new Scalar(255, 255, 255, 0));
                                            Image im = Transform.MatToImageFX(img2);
                                            imageView.setImage(im);
                                        } else {
                                            Exceptions.checkMany();
                                        }
                                    } catch (NumberFormatException e) {
                                        Exceptions.checkEnter();
                                    } catch (CvException e) {
                                        Exceptions.checkMany();
                                    }
                                    stage.close();
                                }
                            });
                        } catch (NullPointerException a) {
                            Exceptions.checkOpen();
                        }
                    }
                });

        shift.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
                            Stage stage = new Stage();
                            stage.setTitle("Enter Shift");
                            Group root = new Group();
                            Scene scene = new Scene(root, 300, 150);
                            Button btn = new Button();
                            btn.setLayoutX(130);
                            btn.setLayoutY(100);
                            btn.setText("OK");
                            Text text1 = new Text();
                            text1.setText("Enter X:");
                            text1.setLayoutX(10);
                            text1.setLayoutY(30);
                            Text text2 = new Text();
                            text2.setText("Enter Y:");
                            text2.setLayoutX(10);
                            text2.setLayoutY(70);
                            TextField input1 = new TextField();
                            input1.setLayoutX(70);
                            input1.setLayoutY(10);
                            TextField input2 = new TextField();
                            input2.setLayoutX(70);
                            input2.setLayoutY(50);
                            root.getChildren().addAll(btn, input1, input2, text1, text2);
                            stage.setScene(scene);
                            Image ico = new Image("mask.png");
                            stage.getIcons().add(ico);
                            stage.show();
                            btn.setOnAction(new EventHandler<ActionEvent>() {
                                public void handle(ActionEvent event) {
                                    try {
                                        double shiftX = Double.parseDouble(input1.getText());
                                        double shiftY = Double.parseDouble(input2.getText());
                                        if(shiftX!=0 && shiftY==0){
                                            Mat M3 = new Mat(2, 3, CvType.CV_32FC1);
                                            M3.put(0, 0,
                                                    1, shiftX, 0,
                                                    0, 1, 0
                                            );
                                            Mat img2 = new Mat();
                                            Imgproc.warpAffine(img, img2, M3,
                                                    new Size(img.width() * (shiftX+1), img.height()),
                                                    Imgproc.INTER_LINEAR, Core.BORDER_CONSTANT,
                                                    new Scalar(255, 255, 255, 0));
                                            Image im = Transform.MatToImageFX(img2);
                                            imageView.setImage(im);
                                        }
                                        else if(shiftY!=0 && shiftX==0){
                                            Mat M3 = new Mat(2, 3, CvType.CV_32FC1);
                                            M3.put(0, 0,
                                                    1, 0, 0,
                                                    shiftY , 1, 0
                                            );
                                            Mat img2 = new Mat();
                                            Imgproc.warpAffine(img, img2, M3,
                                                    new Size(img.width(), img.height()*(shiftY+3)),
                                                    Imgproc.INTER_LINEAR, Core.BORDER_CONSTANT,
                                                    new Scalar(255, 255, 255, 0));
                                            Image im = Transform.MatToImageFX(img2);
                                            imageView.setImage(im);
                                        }
                                        else {
                                            Mat M3 = new Mat(2, 3, CvType.CV_32FC1);
                                            M3.put(0, 0,
                                                    1, 0, 0,
                                                    shiftY , 1, 0
                                            );
                                            Mat img2 = new Mat();
                                            Imgproc.warpAffine(img, img2, M3,
                                                    new Size(img.width(), img.height()*(shiftY+3)),
                                                    Imgproc.INTER_LINEAR, Core.BORDER_CONSTANT,
                                                    new Scalar(255, 255, 255, 0));
                                            Mat M4 = new Mat(2, 3, CvType.CV_32FC1);
                                            M4.put(0, 0,
                                                    1, shiftX, 0,
                                                    0, 1, 0
                                            );
                                            Mat img3 = new Mat();
                                            Imgproc.warpAffine(img2, img3, M4,
                                                    new Size(img2.width()* (shiftX*4), img2.height()),
                                                    Imgproc.INTER_LINEAR, Core.BORDER_CONSTANT,
                                                    new Scalar(255, 255, 255, 0));
                                            Image im = Transform.MatToImageFX(img3);
                                            imageView.setImage(im);
                                        }
                                    } catch (NumberFormatException e) {
                                        Exceptions.checkEnter();
                                    }
                                    stage.close();
                                }
                            });
                        }catch (NullPointerException a) {
                            Exceptions.checkOpen();
                        }
                    }
                });
        rotation.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
                            Stage stage = new Stage();
                            stage.setTitle("Enter Angle");
                            Group root = new Group();
                            Scene scene = new Scene(root, 300, 100);
                            Button btn = new Button();
                            btn.setLayoutX(130);
                            btn.setLayoutY(50);
                            btn.setText("OK");
                            TextField input = new TextField();
                            input.setLayoutX(60);
                            input.setLayoutY(10);
                            root.getChildren().addAll(btn, input);
                            stage.setScene(scene);
                            Image ico = new Image("mask.png");
                            stage.getIcons().add(ico);
                            stage.show();
                            btn.setOnAction(new EventHandler<ActionEvent>() {
                                public void handle(ActionEvent event) {
                                    try {
                                        int angle = Integer.parseInt(input.getText());
                                        Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2BGRA);// Трансформация вращения
                                        Mat M = Imgproc.getRotationMatrix2D(
                                                new Point((float) img.width() / 2, (float) img.height() / 2), angle, 1);// Расчет размеров и положения
                                        Rect rect = new RotatedRect(
                                                new Point((float) img.width() / 2, (float) img.height() / 2),
                                                new Size(img.width(), img.height()), angle).boundingRect();// Корректировка матрицы трансформации
                                        double[] arrX = M.get(0, 2);
                                        double[] arrY = M.get(1, 2);
                                        arrX[0] -= rect.x;
                                        arrY[0] -= rect.y;
                                        M.put(0, 2, arrX);
                                        M.put(1, 2, arrY);// Трансформация
                                        Mat img2 = new Mat();
                                        Imgproc.warpAffine(img, img2, M, rect.size(),
                                                Imgproc.INTER_LINEAR, Core.BORDER_CONSTANT,
                                                new Scalar(255, 255, 255, 0));
                                        Image im = Transform.MatToImageFX(img2);
                                        imageView.setImage(im);

                                    } catch (NumberFormatException e) {
                                        Exceptions.checkEnter();
                                    }
                                    stage.close();
                                }
                            });
                        } catch (NullPointerException a) {
                            Exceptions.checkOpen();
                        }
                    }
                });

        redChannel.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        BufferedImage img;
                        try {
                            img = ImageIO.read(file[0]);
                            int width = img.getWidth();
                            int height = img.getHeight();
                            for (int y = 0; y < height; y++) {
                                for (int x = 0; x < width; x++) {
                                    int p = img.getRGB(x, y);
                                    int a = (p >> 24) & 0xff;
                                    int r = (p >> 16) & 0xff;
                                    p = (a << 24) | (r << 16) | (0);
                                    img.setRGB(x, y, p);
                                }
                            }
                            Image images = SwingFXUtils.toFXImage(img, null);
                            imageView.setImage(images);
                        } catch (Exception e) {
                            Exceptions.checkOpen();
                        }
                    }
                });

        greenChannel.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        BufferedImage img;
                        try {
                            img = ImageIO.read(file[0]);
                            int width = img.getWidth();
                            int height = img.getHeight();
                            for (int y = 0; y < height; y++) {
                                for (int x = 0; x < width; x++) {
                                    int p = img.getRGB(x, y);
                                    int a = (p >> 24) & 0xff;
                                    int g = (p >> 8) & 0xff;
                                    p = (a << 24) | (0) | (g << 8);
                                    img.setRGB(x, y, p);
                                }
                            }
                            Image images = SwingFXUtils.toFXImage(img, null);
                            imageView.setImage(images);
                        } catch (Exception e) {
                            Exceptions.checkOpen();
                        }
                    }
                });

        blueChannel.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        BufferedImage img;
                        try {
                            img = ImageIO.read(file[0]);
                            int width = img.getWidth();
                            int height = img.getHeight();
                            for (int y = 0; y < height; y++) {
                                for (int x = 0; x < width; x++) {
                                    int p = img.getRGB(x, y);
                                    int a = (p >> 24) & 0xff;
                                    int b = p & 0xff;
                                    p = (a << 24) | (0) | b;
                                    img.setRGB(x, y, p);
                                }
                            }
                            Image images = SwingFXUtils.toFXImage(img, null);
                            imageView.setImage(images);
                        } catch (Exception e) {
                            Exceptions.checkOpen();
                        }
                    }
                });
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void setExtFilters(FileChooser chooser) {
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("PNG", "*.png"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg")
        );
    }
}