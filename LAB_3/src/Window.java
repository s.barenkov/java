import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Window {
    public static void windowOpen(Stage stage, ImageView imageView, MenuBar menuBar){

        ScrollPane sp = new ScrollPane(imageView);
        Scene scene = new Scene(new VBox(), 1366, 720);
        sp.setPrefSize(90,scene.getHeight());
        sp.setContent(imageView);
        sp.setPannable(true);
        final VBox vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(10);
        vbox.getChildren().addAll(imageView);
        ((VBox) scene.getRoot()).getChildren().addAll(menuBar, vbox, sp);

        stage.setTitle("LABA 3 Barenkov Sergey");
        stage.setResizable(false);
        Image ico = new Image("mask.png");
        stage.getIcons().add(ico);
        stage.setScene(scene);
        stage.show();
    }
}

