import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.text.Text;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class CheckWindow {

    public static void checkOpen(){
        Stage stage = new Stage();
        stage.setTitle("Error");
        Group root = new Group();
        Scene scene = new Scene(root, 300, 100);
        Button btn = new Button();
        btn.setLayoutX(130);
        btn.setLayoutY(50);
        btn.setText("OK");
        Text t = new Text();
        t.setText("You didn't open the image!");
        t.setLayoutX(70);
        t.setLayoutY(40);
        btn.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                stage.close();
            }
        });
        root.getChildren().addAll(btn, t);
        stage.setScene(scene);
        Image ico = new Image("cat.png");
        stage.getIcons().add(ico);
        stage.setResizable(false);
        stage.setAlwaysOnTop(true);
        stage.show();
    }

    public static void checkMany(){
        Stage stage = new Stage();
        stage.setTitle("Error");
        Group root = new Group();
        Scene scene = new Scene(root, 300, 100);
        Button btn = new Button();
        btn.setLayoutX(130);
        btn.setLayoutY(50);
        btn.setText("OK");
        Text t = new Text();
        t.setText("You entered too many!");
        t.setLayoutX(80);
        t.setLayoutY(40);
        btn.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                stage.close();
            }
        });
        root.getChildren().addAll(btn, t);
        stage.setScene(scene);
        Image ico = new Image("cat.png");
        stage.getIcons().add(ico);
        stage.setResizable(false);
        stage.setAlwaysOnTop(true);
        stage.show();
    }

    public static void checkEnter(){
        Stage stage = new Stage();
        stage.setTitle("Error");
        Group root = new Group();
        Scene scene = new Scene(root, 330, 100);
        Button btn = new Button();
        btn.setLayoutX(150);
        btn.setLayoutY(50);
        btn.setText("OK");
        Text t = new Text();
        t.setText("You entered the wrong number, try again!");
        t.setLayoutX(20);
        t.setLayoutY(40);
        btn.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                stage.close();
            }
        });
        root.getChildren().addAll(btn, t);
        stage.setAlwaysOnTop(true);
        Image ico = new Image("cat.png");
        stage.getIcons().add(ico);
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }
}