import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;


public class Gistagram extends JFrame {

    private static final long serialVersionUID = 1L;
    private JFrame frame = new JFrame();
    public static WorkSpace work = new WorkSpace();
    static BufferedImage bufferedImage;

    public Gistagram(BufferedImage image, String name) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        bufferedImage = image;
        frame.setTitle("Gistagram " + name);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setVisible(true);
        frame.add(work, BorderLayout.CENTER);
        frame.setSize(580, 700);
        work.setVisible(true);
        work.setSize(500, 100);
        gistagram();
    }

    public static void gistagram() {
        Mat mGray = Transformation.bufferedImageToMat(bufferedImage);
        double[] arrG;
        double[] mass = new double[500];
        for (int i = 0; i <= 255; i++) {
            mass[i] = 0;
        }
        double sum = 0;
        for (int i = 0; i < mGray.rows(); i++) {
            for (int j = 0; j < mGray.cols(); j++) {
                arrG = mGray.get(i, j);

                mass[(int) arrG[0]]++;
                sum = sum + arrG[0];
            }
        }
        double max = mass[0];
        for (int i = 0; i <= 255; i++) {
            /*  mass[i]=mass[i]/sum*1000;*/
            if (max < mass[i]) max = mass[i];
        }
        Mat img1 = new Mat(600, 600, CvType.CV_8UC3, new Scalar(255, 255, 255));
        for (int i = 0; i <= 256; i++) {
            Imgproc.line(img1, new Point(i + 10, 600), new Point(i + 10, 600 - mass[i] / max * 550),
                    new Scalar(0), 1, Imgproc.LINE_AA, 0);
        }
        work.setImage(Transformation.matToBufferedImage(img1));
        work.repaint();
    }
}
