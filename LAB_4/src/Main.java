import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import javax.imageio.ImageIO;

public class Main extends Application {

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    @Override
    public void start(Stage stage) {

        File[] file = new File[1];
        ImageView imageView = new ImageView();
        MenuBar menuBar = new MenuBar();
        final FileChooser fileChooser = new FileChooser();
        setExtFilters(fileChooser);
        ScrollPane sp = new ScrollPane(imageView);
        Scene scene = new Scene(new VBox(), 1366, 720);
        sp.setPrefSize(100,scene.getHeight());
        sp.setContent(imageView);
        sp.setPannable(true);
        final VBox vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(10);
        vbox.getChildren().addAll(imageView);
        ((VBox) scene.getRoot()).getChildren().addAll(menuBar, vbox, sp);

        stage.setTitle("LABA 4 Barenkov Sergey");
        stage.setResizable(false);
        Image ico = new Image("cat.png");
        stage.getIcons().add(ico);
        stage.setScene(scene);
        stage.show();

        Menu menuFile = new Menu("Файл");
        MenuItem open = new MenuItem("Открыть");
        MenuItem save = new MenuItem("Сохранть");
        MenuItem close = new MenuItem("Закрыть");
        MenuItem exit = new MenuItem(" Выйти");
        menuFile.getItems().addAll(open, save, close, new SeparatorMenuItem(), exit);
        menuBar.getMenus().addAll(menuFile);

        Menu menuPre = new Menu("букаффки");
        MenuItem one = new MenuItem("а(бинаризация)");
        MenuItem two = new MenuItem("б(яркостный срез 1)");
        MenuItem three = new MenuItem("в(яркостный срез 2(оставляем часть изображения))");
        MenuItem four = new MenuItem("г(Линейное растяжение контраста 1)");
        MenuItem five = new MenuItem("д(Линейное растяжение контраста 2)");
        MenuItem six = new MenuItem("е(Линейное растяжение контраста 3(инверсия для 2))");
        MenuItem seven = new MenuItem("ж(Пилообразное растяжение 1)");
        MenuItem eight = new MenuItem("з(Пилообразное растяжение 2(инверсия 1))");
        MenuItem nine = new MenuItem("и(Пилообразное растяжение 3)");
        MenuItem ten = new MenuItem("к(Пилообразное растяжение 4)");
        menuPre.getItems().addAll(one, two, three, four, five, six, seven, eight, nine, ten);
        menuBar.getMenus().addAll(menuPre);

        Menu gauss = new Menu("Гаусс");
        MenuItem gauss_filter = new MenuItem("Гауссовская фильтрация");
        gauss.getItems().addAll(gauss_filter);
        menuBar.getMenus().addAll(gauss);

        Menu canny = new Menu("Кэнни");
        MenuItem detector = new MenuItem("Нахождение границ методом Кэнни");
        canny.getItems().addAll(detector);
        menuBar.getMenus().addAll(canny);

        open.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(final ActionEvent e) {
                        file[0] = fileChooser.showOpenDialog(stage);
                        try {
                            BufferedImage images = ImageIO.read(file[0]);
                            Image image = SwingFXUtils.toFXImage(images, null);
                            imageView.setImage(image);
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
                            new Gistagram(Transformation.matToBufferedImage(img), "Оригинал");
                        }
                        catch (Exception ex) {
                            imageView.setImage(null);
                            CheckWindow.checkOpen();
                        }
                    }
                });

        save.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        File file = fileChooser.showSaveDialog(stage);
                        if (file!= null) {
                            try {
                                ImageIO.write(SwingFXUtils.fromFXImage(imageView.getImage(),
                                        null), "png", file);
                            } catch (Exception e) {
                                CheckWindow.checkOpen();
                            }
                        }
                    }
                });

        close.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        imageView.setImage(null);
                        file[0]=null;
                    }
                });

        exit.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        System.exit(0);
                    }
                });

        gauss_filter.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try{
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
                            Stage stage = new Stage();
                            stage.setTitle("Введите границы");
                            Group root = new Group();
                            Scene scene = new Scene(root, 300, 230);
                            Button btn = new Button();
                            btn.setLayoutX(130);
                            btn.setLayoutY(180);
                            btn.setText("OK");

                            Text text1 = new Text();
                            text1.setText("Высота:");
                            text1.setLayoutX(10);
                            text1.setLayoutY(30);

                            Text text2 = new Text();
                            text2.setText("Ширина:");
                            text2.setLayoutX(10);
                            text2.setLayoutY(70);

                            Text text3 = new Text();
                            text3.setText("x:");
                            text3.setLayoutX(50);
                            text3.setLayoutY(110);

                            Text text4 = new Text();
                            text4.setText("y:");
                            text4.setLayoutX(50);
                            text4.setLayoutY(150);

                            TextField input1 = new TextField();
                            input1.setLayoutX(90);
                            input1.setLayoutY(10);

                            TextField input2 = new TextField();
                            input2.setLayoutX(90);
                            input2.setLayoutY(50);

                            TextField input3 = new TextField();
                            input3.setLayoutX(90);
                            input3.setLayoutY(90);

                            TextField input4 = new TextField();
                            input4.setLayoutX(90);
                            input4.setLayoutY(130);

                            root.getChildren().addAll(btn, input1, input2, input3, input4, text1, text2, text3, text4);
                            stage.setScene(scene);
                            Image ico = new Image("cat.png");
                            stage.getIcons().add(ico);
                            stage.show();
                            btn.setOnAction(new EventHandler<ActionEvent>() {
                                public void handle(ActionEvent event) {
                                    try {
                                        int height = Integer.parseInt(input1.getText());
                                        int width = Integer.parseInt(input2.getText());
                                        double sigmaX = Double.parseDouble(input3.getText());
                                        double sigmaY = Double.parseDouble(input4.getText());
                                        Mat img2 = new Mat();
                                        Imgproc.GaussianBlur(img, img2, new Size(width, height), sigmaX, sigmaY);
                                        Image im = Transformation.MatToImageFX(img2);
                                        imageView.setImage(im);
                                        new Gistagram(Transformation.matToBufferedImage(img2), "Гаусс");

                                    } catch (NumberFormatException e) {
                                        CheckWindow.checkEnter();
                                    }
                                    catch (CvException c) {
                                        CheckWindow.checkEnter();
                                    }
                                    stage.close();
                                }
                            });
                        }catch (NullPointerException a) {
                            CheckWindow.checkOpen();
                        }
                    }
                });

        one.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
                            Stage stage = new Stage();
                            stage.setTitle("Введите границы");
                            Group root = new Group();
                            Scene scene = new Scene(root, 300, 100);
                            Button btn = new Button();
                            btn.setLayoutX(130);
                            btn.setLayoutY(50);
                            btn.setText("OK");
                            TextField input = new TextField();
                            input.setLayoutX(10);
                            input.setLayoutY(10);
                            root.getChildren().addAll(btn, input);
                            stage.setScene(scene);
                            Image ico = new Image("cat.png");
                            stage.getIcons().add(ico);
                            stage.show();
                            btn.setOnAction(new EventHandler<ActionEvent>() {
                                public void handle(ActionEvent event) {
                                    try {
                                        BufferedImage greyImage = convertImage(Transformation.MatToBufferedImage(img), Imgproc.COLOR_RGB2GRAY);
                                        Mat mGray = Transformation.bufferedImageToMat(greyImage);
                                        int x = Integer.parseInt(input.getText());
                                        double[] arr;
                                        Mat mat;
                                        mat = mGray.clone();
                                        for (int i = 0; i < mat.rows(); i++) {
                                            for (int j = 0; j < mat.cols(); j++) {
                                                arr = mat.get(i, j);
                                                if (arr[0] < x) {
                                                    arr[0] = 0;
                                                } else {
                                                    arr[0] = 255;
                                                }
                                                mat.put(i, j, arr);
                                            }
                                        }
                                        Image im = Transformation.MatToImageFX(mat);
                                        imageView.setImage(im);
                                        new Gistagram(Transformation.matToBufferedImage(mat), "бинаризация");
                                    } catch (NumberFormatException e) {
                                        CheckWindow.checkEnter();
                                    }
                                    stage.close();
                                }
                            });
                        }catch (NullPointerException a) {
                            CheckWindow.checkOpen();
                        }
                    }
                });

        two.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try{
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
                            Stage stage = new Stage();
                            stage.setTitle("Введите границы");
                            Group root = new Group();
                            Scene scene = new Scene(root, 300, 150);
                            Button btn = new Button();
                            btn.setLayoutX(130);
                            btn.setLayoutY(100);
                            btn.setText("OK");
                            Text text1 = new Text();
                            text1.setText("Мин:");
                            text1.setLayoutX(10);
                            text1.setLayoutY(30);
                            Text text2 = new Text();
                            text2.setText("Макс:");
                            text2.setLayoutX(10);
                            text2.setLayoutY(70);
                            TextField input1 = new TextField();
                            input1.setLayoutX(70);
                            input1.setLayoutY(10);
                            TextField input2 = new TextField();
                            input2.setLayoutX(70);
                            input2.setLayoutY(50);
                            root.getChildren().addAll(btn, input1, input2, text1, text2);
                            stage.setScene(scene);
                            Image ico = new Image("cat.png");
                            stage.getIcons().add(ico);
                            stage.show();
                            btn.setOnAction(new EventHandler<ActionEvent>() {
                                public void handle(ActionEvent event) {
                                    try {
                                        BufferedImage greyImage = convertImage(Transformation.MatToBufferedImage(img), Imgproc.COLOR_RGB2GRAY);
                                        Mat mGray = Transformation.bufferedImageToMat(greyImage);
                                        int x1 = Integer.parseInt(input1.getText());
                                        int x2 = Integer.parseInt(input2.getText());
                                        double[] arr;
                                        Mat mat;
                                        mat = mGray.clone();
                                        for (int i = 0; i < mat.rows(); i++) {
                                            for (int j = 0; j < mat.cols(); j++) {
                                                arr = mat.get(i, j);
                                                if (arr[0] > x1 && arr[0] < x2) {
                                                    arr[0] = 255;
                                                } else {
                                                    arr[0] = 0;
                                                }
                                                mat.put(i, j, arr);
                                            }
                                        }
                                        Image im = Transformation.MatToImageFX(mat);
                                        imageView.setImage(im);
                                        new Gistagram(Transformation.matToBufferedImage(mat), "яркостный срез 1");
                                    } catch (NumberFormatException e) {
                                        CheckWindow.checkEnter();
                                    }
                                    stage.close();
                                }
                            });
                        }catch (NullPointerException a) {
                            CheckWindow.checkOpen();
                        }
                    }
                });

        three.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try{
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
                            Stage stage = new Stage();
                            stage.setTitle("Введие границы");
                            Group root = new Group();
                            Scene scene = new Scene(root, 300, 150);
                            Button btn = new Button();
                            btn.setLayoutX(130);
                            btn.setLayoutY(100);
                            btn.setText("OK");
                            Text text1 = new Text();
                            text1.setText("Мин:");
                            text1.setLayoutX(10);
                            text1.setLayoutY(30);
                            Text text2 = new Text();
                            text2.setText("Макс:");
                            text2.setLayoutX(10);
                            text2.setLayoutY(70);
                            TextField input1 = new TextField();
                            input1.setLayoutX(70);
                            input1.setLayoutY(10);
                            TextField input2 = new TextField();
                            input2.setLayoutX(70);
                            input2.setLayoutY(50);
                            root.getChildren().addAll(btn, input1, input2, text1, text2);
                            stage.setScene(scene);
                            Image ico = new Image("cat.png");
                            stage.getIcons().add(ico);
                            stage.show();
                            btn.setOnAction(new EventHandler<ActionEvent>() {
                                public void handle(ActionEvent event) {
                                    try {
                                        BufferedImage greyImage = convertImage(Transformation.MatToBufferedImage(img), Imgproc.COLOR_RGB2GRAY);
                                        Mat mGray = Transformation.bufferedImageToMat(greyImage);
                                        int x1 = Integer.parseInt(input1.getText());
                                        int x2 = Integer.parseInt(input2.getText());
                                        double[] arr;
                                        Mat mat;
                                        mat = mGray.clone();
                                        for (int i = 0; i < mat.rows(); i++) {
                                            for (int j = 0; j < mat.cols(); j++) {
                                                arr = mat.get(i, j);
                                                if (arr[0] > x1 && arr[0] < x2) {
                                                    arr[0] = 255;
                                                }
                                                mat.put(i, j, arr);
                                            }
                                        }
                                        Image im = Transformation.MatToImageFX(mat);
                                        imageView.setImage(im);
                                        new Gistagram(Transformation.matToBufferedImage(mat), "яркостный срез 2");

                                    } catch (NumberFormatException e) {
                                        CheckWindow.checkEnter();
                                    }
                                    stage.close();
                                }
                            });
                        }catch (NullPointerException a) {
                            CheckWindow.checkOpen();
                        }
                    }
                });

        four.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
                            Stage stage = new Stage();
                            stage.setTitle("Введите границы");
                            Group root = new Group();
                            Scene scene = new Scene(root, 300, 100);
                            Button btn = new Button();
                            btn.setLayoutX(130);
                            btn.setLayoutY(50);
                            btn.setText("OK");
                            TextField input = new TextField();
                            input.setLayoutX(60);
                            input.setLayoutY(10);
                            root.getChildren().addAll(btn, input);
                            stage.setScene(scene);
                            Image ico = new Image("cat.png");
                            stage.getIcons().add(ico);
                            stage.show();
                            btn.setOnAction(new EventHandler<ActionEvent>() {
                                public void handle(ActionEvent event) {
                                    try {
                                        BufferedImage greyImage = convertImage(Transformation.MatToBufferedImage(img), Imgproc.COLOR_RGB2GRAY);
                                        Mat mGray = Transformation.bufferedImageToMat(greyImage);
                                        int x = Integer.parseInt(input.getText());
                                        double[] arr;
                                        Mat mat;
                                        mat = mGray.clone();
                                        for (int i = 0; i < mat.rows(); i++) {
                                            for (int j = 0; j < mat.cols(); j++) {
                                                arr = mGray.get(i, j);
                                                if (arr[0] > x) {
                                                    arr[0] = 255;
                                                }
                                                mat.put(i, j, arr);
                                            }
                                        }
                                        Image im = Transformation.MatToImageFX(mat);
                                        imageView.setImage(im);
                                        new Gistagram(Transformation.matToBufferedImage(mat), "Линейное растяжение контраста 1");

                                    } catch (NumberFormatException e) {
                                        CheckWindow.checkEnter();
                                    }
                                    stage.close();
                                }
                            });
                        }catch (NullPointerException a) {
                            CheckWindow.checkOpen();
                        }
                    }
                });

        five.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try{
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
//                            Stage stage = new Stage();
//                            stage.setTitle("Enter borders");
//                            Group root = new Group();
//                            Scene scene = new Scene(root, 300, 150);
//                            Button btn = new Button();
//                            btn.setLayoutX(130);
//                            btn.setLayoutY(100);
//                            btn.setText("OK");
//                            Text text1 = new Text();
//                            text1.setText("Enter min:");
//                            text1.setLayoutX(10);
//                            text1.setLayoutY(30);
//                            Text text2 = new Text();
//                            text2.setText("Enter max:");
//                            text2.setLayoutX(10);
//                            text2.setLayoutY(70);
//                            TextField input1 = new TextField();
//                            input1.setLayoutX(70);
//                            input1.setLayoutY(10);
//                            TextField input2 = new TextField();
//                            input2.setLayoutX(70);
//                            input2.setLayoutY(50);
//                            root.getChildren().addAll(btn, input1, input2, text1, text2);
//                            stage.setScene(scene);
//                            Image ico = new Image("mac.png");
//                            stage.getIcons().add(ico);
//                            stage.show();
//                            btn.setOnAction(new EventHandler<ActionEvent>() {
//                                public void handle(ActionEvent event) {
//                                    try {
                                        BufferedImage greyImage = convertImage(Transformation.MatToBufferedImage(img), Imgproc.COLOR_RGB2GRAY);
                                        Mat mGray = Transformation.bufferedImageToMat(greyImage);
                                        int x1 = 180;
                                        int x2 = 240;
                                        double[] arr;
                                        Mat mat;
                                        mat = mGray.clone();
                                        int dim = x2 - x1;
                                        int k = 255 / (x2 * dim / 255);
                                        for (int i = 0; i < mat.rows(); i++) {
                                            for (int j = 0; j < mat.cols(); j++) {
                                                arr = mat.get(i, j);
                                                if (arr[0] < x1) {
                                                    arr[0] = 0;
                                                } else if
                                                (arr[0] > x2) {
                                                    arr[0] = 255;
                                                } else {
                                                    arr[0] = 255 * (arr[0] - x1) / (x2 - x1);
                                                }
                                                mat.put(i, j, arr);
                                            }
                                        }
                                        Image im = Transformation.MatToImageFX(mat);
                                        imageView.setImage(im);
                                        new Gistagram(Transformation.matToBufferedImage(mat), "Линейное растяжение контраста 2");

                                    } catch (NumberFormatException e) {
                                        CheckWindow.checkEnter();
                                    }
//                                    stage.close();
//                                }
//                            });
//                        }
                        catch (NullPointerException a) {
                            CheckWindow.checkOpen();
                        }
                    }
                });

        six.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try{
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
//                            Stage stage = new Stage();
//                            stage.setTitle("Enter borders");
//                            Group root = new Group();
//                            Scene scene = new Scene(root, 300, 150);
//                            Button btn = new Button();
//                            btn.setLayoutX(130);
//                            btn.setLayoutY(100);
//                            btn.setText("OK");
//                            Text text1 = new Text();
//                            text1.setText("Enter min:");
//                            text1.setLayoutX(10);
//                            text1.setLayoutY(30);
//                            Text text2 = new Text();
//                            text2.setText("Enter max:");
//                            text2.setLayoutX(10);
//                            text2.setLayoutY(70);
//                            TextField input1 = new TextField();
//                            input1.setLayoutX(70);
//                            input1.setLayoutY(10);
//                            TextField input2 = new TextField();
//                            input2.setLayoutX(70);
//                            input2.setLayoutY(50);
//                            root.getChildren().addAll(btn, input1, input2, text1, text2);
//                            stage.setScene(scene);
//                            Image ico = new Image("mac.png");
//                            stage.getIcons().add(ico);
//                            stage.show();
//                            btn.setOnAction(new EventHandler<ActionEvent>() {
//                                public void handle(ActionEvent event) {
//                                    try {
                                        BufferedImage greyImage = convertImage(Transformation.MatToBufferedImage(img), Imgproc.COLOR_RGB2GRAY);
                                        Mat mGray = Transformation.bufferedImageToMat(greyImage);
                                        int x1 = 180;
                                        int x2 = 240;
                                        double[] arr;
                                        Mat mat;
                                        mat = mGray.clone();
                                        for (int i = 0; i < mat.rows(); i++) {
                                            for (int j = 0; j < mat.cols(); j++) {
                                                arr = mat.get(i, j);
                                                if (arr[0] < x1) {
                                                    arr[0] = 255;
                                                } else if
                                                (arr[0] > x2) {
                                                    arr[0] = 0;
                                                } else {
                                                    arr[0] = 255 - (255 * (arr[0] - x1) / (x2 - x1));
                                                }
                                                mat.put(i, j, arr);
                                            }
                                        }
                                        Image im = Transformation.MatToImageFX(mat);
                                        imageView.setImage(im);
                                        new Gistagram(Transformation.matToBufferedImage(mat), "Линейное растяжение контраста 3");

                                    } catch (NumberFormatException e) {
                                        CheckWindow.checkEnter();
                                    }
//                                    stage.close();
//                                }
//                            });
//                        }
                        catch (NullPointerException a) {
                            CheckWindow.checkOpen();
                        }
                    }
                });

        seven.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try{
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
                            Stage stage = new Stage();
                            stage.setTitle("Введите границы");
                            Group root = new Group();
                            Scene scene = new Scene(root, 300, 150);
                            Button btn = new Button();
                            btn.setLayoutX(130);
                            btn.setLayoutY(100);
                            btn.setText("OK");
                            Text text1 = new Text();
                            text1.setText("Мин:");
                            text1.setLayoutX(10);
                            text1.setLayoutY(30);
                            Text text2 = new Text();
                            text2.setText("Макс:");
                            text2.setLayoutX(10);
                            text2.setLayoutY(70);
                            TextField input1 = new TextField();
                            input1.setLayoutX(70);
                            input1.setLayoutY(10);
                            TextField input2 = new TextField();
                            input2.setLayoutX(70);
                            input2.setLayoutY(50);
                            root.getChildren().addAll(btn, input1, input2, text1, text2);
                            stage.setScene(scene);
                            Image ico = new Image("cat.png");
                            stage.getIcons().add(ico);
                            stage.show();
                            btn.setOnAction(new EventHandler<ActionEvent>() {
                                public void handle(ActionEvent event) {
                                    try {
                                        BufferedImage greyImage = convertImage(Transformation.MatToBufferedImage(img), Imgproc.COLOR_RGB2GRAY);
                                        Mat mGray = Transformation.bufferedImageToMat(greyImage);
                                        int x1 = Integer.parseInt(input1.getText());
                                        int x2 = Integer.parseInt(input2.getText());
                                        double[] arr;
                                        Mat mat;
                                        mat = mGray.clone();
                                        for (int i = 0; i < mat.rows(); i++) {
                                            for (int j = 0; j < mat.cols(); j++) {
                                                arr = mat.get(i, j);
                                                if (arr[0] < x1) {
                                                    arr[0] = 0;
                                                } else if
                                                (arr[0] > x2) {
                                                    arr[0] = 0;
                                                } else {
                                                    arr[0] = 255 * (arr[0] - x1) / (x2 - x1);
                                                }
                                                mat.put(i, j, arr);
                                            }
                                        }
                                        Image im = Transformation.MatToImageFX(mat);
                                        imageView.setImage(im);
                                        new Gistagram(Transformation.matToBufferedImage(mat), "Пилообразное растяжение контраста");

                                    } catch (NumberFormatException e) {
                                        CheckWindow.checkEnter();
                                    }
                                    stage.close();
                                }
                            });
                        }catch (NullPointerException a) {
                            CheckWindow.checkOpen();
                        }
                    }
                });

        eight.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try{
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
                            Stage stage = new Stage();
                            stage.setTitle("Введите границы");
                            Group root = new Group();
                            Scene scene = new Scene(root, 300, 150);
                            Button btn = new Button();
                            btn.setLayoutX(130);
                            btn.setLayoutY(100);
                            btn.setText("OK");
                            Text text1 = new Text();
                            text1.setText("Мин:");
                            text1.setLayoutX(10);
                            text1.setLayoutY(30);
                            Text text2 = new Text();
                            text2.setText("Макс:");
                            text2.setLayoutX(10);
                            text2.setLayoutY(70);
                            TextField input1 = new TextField();
                            input1.setLayoutX(70);
                            input1.setLayoutY(10);
                            TextField input2 = new TextField();
                            input2.setLayoutX(70);
                            input2.setLayoutY(50);
                            root.getChildren().addAll(btn, input1, input2, text1, text2);
                            stage.setScene(scene);
                            Image ico = new Image("cat.png");
                            stage.getIcons().add(ico);
                            stage.show();
                            btn.setOnAction(new EventHandler<ActionEvent>() {
                                public void handle(ActionEvent event) {
                                    try {
                                        BufferedImage greyImage = convertImage(Transformation.MatToBufferedImage(img), Imgproc.COLOR_RGB2GRAY);
                                        Mat mGray = Transformation.bufferedImageToMat(greyImage);
                                        int x1 = Integer.parseInt(input1.getText());
                                        int x2 = Integer.parseInt(input2.getText());
                                        double[] arr;
                                        Mat mat;
                                        mat = mGray.clone();
                                        for (int i = 0; i < mat.rows(); i++) {
                                            for (int j = 0; j < mat.cols(); j++) {
                                                arr = mat.get(i, j);
                                                if (arr[0] < x1) {
                                                    arr[0] = 255;
                                                } else if
                                                (arr[0] > x2) {
                                                    arr[0] = 255;
                                                } else {
                                                    arr[0] = 255 * (arr[0] - x1) / (x2 - x1);
                                                }
                                                mat.put(i, j, arr);
                                            }
                                        }
                                        Image im = Transformation.MatToImageFX(mat);
                                        imageView.setImage(im);
                                        new Gistagram(Transformation.matToBufferedImage(mat), "Пилообразное растяжение контраста 2");

                                    } catch (NumberFormatException e) {
                                        CheckWindow.checkEnter();
                                    }
                                    stage.close();
                                }
                            });
                        }catch (NullPointerException a) {
                            CheckWindow.checkOpen();
                        }
                    }
                });

        nine.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try{
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
                            Stage stage = new Stage();
                            stage.setTitle("Введите границы");
                            Group root = new Group();
                            Scene scene = new Scene(root, 300, 150);
                            Button btn = new Button();
                            btn.setLayoutX(130);
                            btn.setLayoutY(100);
                            btn.setText("OK");
                            Text text1 = new Text();
                            text1.setText("Мин:");
                            text1.setLayoutX(10);
                            text1.setLayoutY(30);
                            Text text2 = new Text();
                            text2.setText("Макс:");
                            text2.setLayoutX(10);
                            text2.setLayoutY(70);
                            TextField input1 = new TextField();
                            input1.setLayoutX(70);
                            input1.setLayoutY(10);
                            TextField input2 = new TextField();
                            input2.setLayoutX(70);
                            input2.setLayoutY(50);
                            root.getChildren().addAll(btn, input1, input2, text1, text2);
                            stage.setScene(scene);
                            Image ico = new Image("cat.png");
                            stage.getIcons().add(ico);
                            stage.show();
                            btn.setOnAction(new EventHandler<ActionEvent>() {
                                public void handle(ActionEvent event) {
                                    try {
                                        BufferedImage greyImage = convertImage(Transformation.MatToBufferedImage(img), Imgproc.COLOR_RGB2GRAY);
                                        Mat mGray = Transformation.bufferedImageToMat(greyImage);
                                        int x1 = Integer.parseInt(input1.getText());
                                        int x2 = Integer.parseInt(input2.getText());
                                        double[] arr;
                                        Mat mat;
                                        mat = mGray.clone();
                                        for (int i = 0; i < mat.rows(); i++) {
                                            for (int j = 0; j < mat.cols(); j++) {
                                                arr = mat.get(i, j);
                                                if (arr[0] < x1) {
                                                    arr[0] = 128;
                                                } else if
                                                (arr[0] > x2) {
                                                    arr[0] = 128;
                                                } else {
                                                    arr[0] = 255 * (arr[0] - x1) / (x2 - x1);
                                                }
                                                mat.put(i, j, arr);
                                            }
                                        }
                                        Image im = Transformation.MatToImageFX(mat);
                                        imageView.setImage(im);
                                        new Gistagram(Transformation.matToBufferedImage(mat), "Пилообразное растяжение контраста 3");

                                    } catch (NumberFormatException e) {
                                        CheckWindow.checkEnter();
                                    }
                                    stage.close();
                                }
                            });
                        }catch (NullPointerException a) {
                            CheckWindow.checkOpen();
                        }
                    }
                });

        ten.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
                            Stage stage = new Stage();
                            stage.setTitle("Введите границы");
                            Group root = new Group();
                            Scene scene = new Scene(root, 300, 100);
                            Button btn = new Button();
                            btn.setLayoutX(130);
                            btn.setLayoutY(50);
                            btn.setText("OK");
                            TextField input = new TextField();
                            input.setLayoutX(60);
                            input.setLayoutY(10);
                            root.getChildren().addAll(btn, input);
                            stage.setScene(scene);
                            Image ico = new Image("cat.png");
                            stage.getIcons().add(ico);
                            stage.show();
                            btn.setOnAction(new EventHandler<ActionEvent>() {
                                public void handle(ActionEvent event) {
                                    try {
                                        BufferedImage greyImage = convertImage(Transformation.MatToBufferedImage(img), Imgproc.COLOR_RGB2GRAY);
                                        Mat mGray = Transformation.bufferedImageToMat(greyImage);
                                        int n = Integer.parseInt(input.getText());
                                        double[] arr;
                                        int dim = (int) Math.floor(256 / n);
                                        int x1 = 0;
                                        int x2 = dim - 1;
                                        Mat mat;
                                        mat = mGray.clone();
                                        for (int i = 0; i < mat.rows(); i++) {
                                            for (int j = 0; j < mat.cols(); j++) {
                                                arr = mat.get(i, j);
                                                int pldim = 0;
                                                while (arr[0] > dim) {
                                                    arr[0] = arr[0] - dim;
                                                    pldim++;
                                                }
                                                arr[0]++;
                                                arr[0] = 255 * (arr[0] - x1) / (x2 - x1);
                                                while (pldim > 0) {
                                                    arr[0] = arr[0] + dim;
                                                    pldim--;
                                                }
//                                                arr[0]=(int)((arr[0] - (dim)*Math.floor(arr[0]/dim)) * 255/dim);
                                                mat.put(i, j, arr);
                                            }
                                        }
                                        Image im = Transformation.MatToImageFX(mat);
                                        imageView.setImage(im);
                                        new Gistagram(Transformation.matToBufferedImage(mat), "Пилообразное растяжение контрастирования 3");

                                    } catch (NumberFormatException e) {
                                        CheckWindow.checkEnter();
                                    }
                                    stage.close();
                                }
                            });
                        }catch (NullPointerException a) {
                            CheckWindow.checkOpen();
                        }
                    }
                });

        detector.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try{
                            Mat img = Imgcodecs.imread(file[0].toPath().toString());
                            Stage stage = new Stage();
                            stage.setTitle("Введите границы фильтрации");
                            Group root = new Group();
                            Scene scene = new Scene(root, 300, 150);
                            Button btn = new Button();
                            btn.setLayoutX(130);
                            btn.setLayoutY(100);
                            btn.setText("OK");

                            Text text1 = new Text();
                            text1.setText("Мин:");
                            text1.setLayoutX(10);
                            text1.setLayoutY(30);

                            Text text2 = new Text();
                            text2.setText("Макс:");
                            text2.setLayoutX(10);
                            text2.setLayoutY(70);

                            TextField input1 = new TextField();
                            input1.setLayoutX(120);
                            input1.setLayoutY(10);

                            TextField input2 = new TextField();
                            input2.setLayoutX(120);
                            input2.setLayoutY(50);

                            root.getChildren().addAll(btn, input1, input2, text1, text2);
                            stage.setScene(scene);
                            Image ico = new Image("cat.png");
                            stage.getIcons().add(ico);
                            stage.show();
                            btn.setOnAction(new EventHandler<ActionEvent>() {
                                public void handle(ActionEvent event) {
                                    try {
                                        int x1 = Integer.parseInt(input1.getText());
                                        int x2 = Integer.parseInt(input2.getText());
                                        Mat imgGray = new Mat();
                                        Imgproc.cvtColor(img, imgGray, Imgproc.COLOR_BGR2GRAY);
                                        Mat edges = new Mat();
                                        Imgproc.Canny(imgGray, edges, x1, x2);
                                        Image im = Transformation.MatToImageFX(edges);
                                        imageView.setImage(im);
                                        new Gistagram(Transformation.matToBufferedImage(edges), "Кэнни");

                                    } catch (NumberFormatException e) {
                                        CheckWindow.checkEnter();
                                    }
                                    stage.close();
                                }
                            });
                        }catch (NullPointerException a) {
                            CheckWindow.checkOpen();
                        }
                    }
                });
    }

    public static BufferedImage convertImage(BufferedImage img, int imgProc) {
        Mat mat = Transformation.bufferedImageToMat(img);
        Mat new_mat = new Mat();
        Imgproc.cvtColor(mat, new_mat, imgProc);
        BufferedImage inputImage = Transformation.matToBufferedImage(new_mat);
        return inputImage;
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void setExtFilters(FileChooser chooser){
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("PNG", "*.png"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg")
        );
    }
}