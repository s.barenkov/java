package laba;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        boolean check = true;
        Scanner sc = new Scanner(System.in);
        while (check){
            System.out.println("Выберите номер задания от 1 до 40 или же нажмите 0 для выхода");
            int n = sc.nextInt();
            switch (n){
                case 1: zad1.main();break;
                case 2: zad2.main();break;
                case 3: zad3.main();break;
                case 4: zad4.main();break;
                case 5: zad5.main();break;
                case 6: zad6.main();break;
                case 7: zad7.main();break;
                case 8: zad8.main();break;
                case 9: zad9.main();break;
                case 10: zad10.main();break;
                case 11: zad11.main();break;
                case 12: zad12.main();break;
                case 13: zad13.main();break;
                case 14: zad14.main();break;
                case 15: zad15.main();break;
                case 16: zad16.main();break;
                case 17: zad17.main();break;
                case 18: zad18.main();break;
                case 19: zad19.main();break;
                case 20: zad20.main();break;
                case 21: zad21.main();break;
                case 22: zad22.main();break;
                case 23: zad23.main();break;
                case 24: zad24.main();break;
                case 25: zad25.main();break;
                case 26: zad26.main();break;
                case 27: zad27.main();break;
                case 28: zad28.main();break;
                case 29: zad29.main();break;
                case 30: zad30.main();break;
                case 31: zad31.main();break;
                case 32: zad32.main();break;
                case 33: zad33.main();break;
                case 34: zad34.main();break;
                case 35: zad35.main();break;
                case 36: zad36.main();break;
                case 37: zad37.main();break;
                case 38: zad38.main();break;
                case 39: zad39.main();break;
                case 40: zad40.main();break;
                case 0: check = false;break;
                default:
                    System.out.println("Нету такой задачи попробуйте снова");
            }
        }
    }
}
