package laba;

import java.util.Scanner;

public class zad13 {
    public static void main(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите натуральное число");
        int n;
        if (sc.hasNextInt()){
            n= sc.nextInt();
            for (int i=n; i>0; i--){
                int del=n%i;
                if (del==0){
                    System.out.println(i);
                }
            }
        }
        else {
            System.out.println("Вы ввели не натуральное число или строку");
        }
    }
}
