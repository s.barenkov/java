package laba;

import java.util.Scanner;

public class zad14 {
    public static void main(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите натуральное число: ");
        int n;
        if (sc.hasNextInt()){
            n = sc.nextInt();
            for(int i=(int)Math.sqrt(n);i>=1;i--){
                if(n % i == 0 & i != 1){
                    System.out.println("Число является составным");
                    break;
                }
                if(i==1) System.out.println("Число является простым");
            }
        }
        else System.out.println("Вы ввели не натуральное число");
    }
}
