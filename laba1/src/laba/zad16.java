package laba;

import java.util.Scanner;

public class zad16 {
    public static void main(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите натуральное число: ");
        int n;
        if (sc.hasNextInt()){
            n = sc.nextInt();
            int sum=0;
            while (n!=0){
                sum += n % 10;
                n /= 10;
            }
            System.out.println(sum);
        }
        else System.out.println("Вы ввели не натуральное число!");
    }

    public static int CheckNumberOfValue(int num){
        return String.valueOf(Math.abs(num)).length();
    }
}
