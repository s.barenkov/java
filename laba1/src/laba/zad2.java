package laba;

import java.util.Scanner;

public class zad2 {
    public static void main(){
      boolean check = true;
      Scanner sc = new Scanner(System.in);
        String menu = "Меню выбора задания\n" + "1 - Суммы половинок четырехзначного числа равны\n" + "2 - Четная сумма цифр\n" + "3 - четность двузначного числа\n"
                + "4 - Пифагор\n" + "5 - Различность цифр\n" + "6 - первая цифра равна последней и 2=3\n" + "0 - Выход\n";
      while (check){
          System.out.println(menu);
          int n = sc.nextInt();
          switch (n){
              case 1:tip1();break;
              case 2:tip2();break;
              case 3:tip3();break;
              case 4:tip4();break;
              case 5:tip5();break;
              case 6:tip6();break;
              case 0: check = false; break;
              default:System.out.println("Такого задания нету");
          }
      }
    }

    public static void tip1(){
        Scanner sc = new Scanner(System.in);
        System.out.println("(суммы половинок равны)Введите четырехзначное число: ");
        int n;
        int first, second, third, fourth;
        if (sc.hasNextInt()){
            n = sc.nextInt();
            if (4 == Checklength(n)){
                first = n / 1000;
                second = n / 100 % 10;
                third = n % 100 / 10;
                fourth = n % 1000 % 10;
                if (first + second == third + fourth){
                    System.out.println("True");
                }
                else {
                    System.out.println("False");
                }
            }
            else{
                System.out.println("Вы ввели не четырехзначное число");
            }
        }
        else{
            System.out.println("Вы ввели строку или не целое число");
        }
    }


    public static void tip2(){
        Scanner sc = new Scanner(System.in);
        System.out.println("(Четная сумма цифр)Введите трехзначное число: ");
        int n, sum=0;
        if (sc.hasNextInt()){
            n = sc.nextInt();
            if (3 == Checklength(n)){
                while(n!=0) {
                    sum += (n % 10);
                    n /= 10;
                }
                if (sum % 2 == 0){
                    System.out.println("True");
                }
                else {
                    System.out.println("False");
                }
            }
            else {
                System.out.println("Вы ввели не трехзначное число");
            }
        }
        else{
            System.out.println("Вы ввели строку или не целое число");
        }
    }

    public static void tip3(){
        Scanner sc = new Scanner(System.in);
        System.out.println("(Четность и двузначность)Введите двухзначное число: ");
        int n;
        if (sc.hasNextInt()){
            n = sc.nextInt();
            if (Checklength(n)==2){
                if (n % 2 == 0){
                    System.out.println("True");
                }
                else{
                    System.out.println("False (число не четное)");
                }
            }
            else{
                System.out.println("False (Вы ввели не двухзначное число)");
            }
        }
        else {
            System.out.println("Вы ввели строку или не целое число");
        }
    }

    public static void tip4(){
        Scanner sc = new Scanner(System.in);
        System.out.println("(Пифагор)Введите числа: ");
        int a,b,c;
        System.out.println("Введите число a: ");
        if (sc.hasNextInt()){
            a = sc.nextInt();
            System.out.println("Введите число b: ");
            b = sc.nextInt();
            System.out.println("Введите число c: ");
            c = sc.nextInt();
            a = (int) Math.pow(a,2);
            b = (int) Math.pow(b,2);
            c = (int) Math.pow(c,2);
            if (c == a+b){
                System.out.println("True");
            }
            else {
                System.out.println("False");
            }
        }
        else {
            System.out.println("Вы ввели строку или не целое число");
        }
    }

    public static void tip5(){
        Scanner sc = new Scanner(System.in);
        System.out.println("(Проверка на различность цифр)Введите четырехзначное число: ");
        int n;
        int first, second, third, fourth;
        if (sc.hasNextInt()){
            n = sc.nextInt();
            if (4 == Checklength(n)){
                first = n / 1000;
                second = n / 100 % 10;
                third = n % 100 / 10;
                fourth = n % 100 % 10;
                if (first != second && first != third && first != fourth && second != third && second != fourth && third != fourth){
                    System.out.println("True");
                }
                else {
                    System.out.println("False");
                }
            }
            else{
                System.out.println("Вы ввели не четырехзначное число");
            }
        }
        else{
            System.out.println("Вы ввели строку или не целое число");
        }
    }

    public static void tip6(){
        Scanner sc = new Scanner(System.in);
        System.out.println("(Проверка на 1221)Введите четырехзначное число: ");
        int n;
        int first, second, third, fourth;
        if (sc.hasNextInt()){
            n = sc.nextInt();
            if (4 == Checklength(n)){
                first = n / 1000;
                second = n / 100 % 10;
                third = n % 100 / 10;
                fourth = n % 100 % 10;
                if (first == fourth && second == third){
                    System.out.println("True");
                }
                else {
                    System.out.println("False");
                }
            }
            else{
                System.out.println("Вы ввели не четырехзначное число");
            }
        }
        else{
            System.out.println("Вы ввели строку или не целое число");
        }
    }

    public static int Checklength(long number) {
        return String.valueOf(Math.abs(number)).length();
    }
}
