package laba;

import java.util.Random;
import java.util.Scanner;

public class zad29 {
    public static void main(){
        Random rand = new Random();
        Scanner sc = new Scanner(System.in);
        boolean run = true;
        int right=0,left=0;
        while (run){
            System.out.println("Введите четное число n: ");
            if (sc.hasNextInt()){
                int n = sc.nextInt();
                if (n % 2 == 0 || n < 0){
                    int[] arr = new int[n];
                    for (int i = 0; i< arr.length; i++){
                        arr[i] = rand.nextInt(11)-5;
                        System.out.print(arr[i] + " ");
                    }
                    for (int i = 0; i< arr.length; i++){
                        if (i<= arr.length/2-1){
                            left += Math.abs(arr[i]);
                        }
                        else {
                            right += Math.abs(arr[i]);
                        }
                    }
                    if (left>right) System.out.println("\nСумма модулей левой половины больше");
                    else if (left<right) System.out.println("\nСумма модулей правой половины больше");
                    else System.out.println("\nСумма модулей правой и левой половин равны");
                    run = false;
                }
                else {
                    System.out.println("Вы ввели не четное или отрицательное число число попробуйте снова");
                }
            }
            else{
                System.out.println("Вы ввели не целое число попробуйте снова");
            }
        }
    }
}
