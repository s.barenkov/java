package laba;

import java.util.Scanner;

public class zad3 {
    public static void main(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите два действительных числа: ");
        double x,y;
        if (sc.hasNextDouble()){
            x = sc.nextDouble();
            y = sc.nextDouble();
            if (x>y){
                x = 2*(x*y);
                System.out.println(x);
            }
            else{
                y = (y+x)/2;
                System.out.println(y);
            }
        }
        else {
            System.out.println("Вы ввели не действительное число");
        }
    }
}
