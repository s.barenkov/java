package laba;

import java.awt.*;
import java.util.Random;
import java.util.Scanner;

public class zad31 {
    public static void main(){
        Random rand = new Random();
        Scanner sc = new Scanner(System.in);
        boolean run = true;
        int even;
        while (run){
            System.out.println("Введите число n");
            if (sc.hasNextInt()){
                int n = sc.nextInt();
                if (n>=3){
                    int[] arr1 = new int[n];
                    for (int i = 0; i < arr1.length; i++){
                        arr1[i] = rand.nextInt(n+1);
                        System.out.print(arr1[i] + " ");
                    }
                    System.out.println();
                    int[] arr2 = new int[n];
                    for (int i = 0; i < arr1.length; i++){
                        even = arr1[i];
                        if (even%2==0){
                            arr2[i] = even;
                            System.out.print(arr2[i] + " ");
                        }
                    }
                    run = false;
                }
                else System.out.println("Вы ввели число меньше 3 попробуйте снова");
            }
            else System.out.println("Вы ввели не целое число попробуйте снова");
        }
    }
}
