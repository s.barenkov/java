package laba;

import java.util.Random;
import java.util.Scanner;

public class zad36 {
    public static void main(){
        Random rand = new Random();
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()){
            int n = sc.nextInt();
            int[][] arr = new int[n][n];
            for (int i = 0; i < arr.length; i++){
                for (int j = 0; j < arr[i].length; j++){
                    arr[i][j] = rand.nextInt(20)-10;
                    System.out.print(arr[i][j] + " ");
                }
                System.out.println();
            }
            int max =0, MaxIndex= 0, index;
            max=Math.abs(arr[0][0]);
            for (int i = 1; i < arr.length; i++){
                if (arr[i][i]>max){
                    max = arr[i][i];
                    MaxIndex = i;
                }
            }
            System.out.println("Выберите строку: ");
            index = sc.nextInt();
            int[] arrTemp = new int[n];
            for(int j = 0; j < arr[MaxIndex].length; j++) {
                arrTemp[j] = arr[MaxIndex][j];
                arr[MaxIndex][j] = arr[index][j];
                arr[index][j] = arrTemp[j];
            }
            for (int i[] : arr){
                for (int j : i){
                    System.out.print(j + " ");
                }
                System.out.println();
            }
        }
    }
}
