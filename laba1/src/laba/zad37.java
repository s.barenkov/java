package laba;

import java.util.Random;
import java.util.Scanner;

public class zad37 {
    public static void main(){
        Random rand = new Random();
        Scanner sc = new Scanner(System.in);
        int max = 0, min = 0, temp = 0, indexMax=0, indexMin=0;
        if (sc.hasNextInt()){
            int n = sc.nextInt();
            int m = sc.nextInt();
            int[][] B = new int[n][m];
            for (int i = 0; i < n; i++){
                for (int j = 0; j < m; j++){
                    B[i][j] = rand.nextInt(20);
                    System.out.print(B[i][j] + " ");
                }
                System.out.println();
            }
            for (int i = 0; i < n; i++){
                max = B[i][0];
                min = B[i][0];
                for (int j = 0; j < m; j++){
                   if (max<B[i][j]){
                       max = B[i][j];
                       indexMax = j;
                   }
                   if (min>B[i][j]){
                       min = B[i][j];
                       indexMin = j;
                   }
                }
                B[i][indexMin] = B[i][0];
                B[i][0] = min;
                B[i][indexMax] = B[i][m-1];
                B[i][m-1] = max;
            }
            System.out.println();
            for (int i[] : B){
                for (int j : i){
                    System.out.print(j + " ");
                }
                System.out.println();
            }
        }
    }
}
