package laba;

import java.util.Scanner;

public class zad38 {
    public static void main(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите размерность матрицы n: ");
        int n = sc.nextInt();
        if (n%2!=0){
            System.out.println("Нечетный квадрат: ");
            odd(n);
        }
        else if(n%4==0){
            System.out.println("Четный квадрат: ");
            even(n);
        }
        else if((n==0)||(n==2)||(n%4!=0)){
            System.out.println("Невозможно составить матрицу");
        }
    }

    public static void odd(int n){
        int[][] square = new int[n][n];
        int i=n/2,j=n-1;//позиция первого
        for (int num =1;num<=Math.pow(n,2);){
            if (i == -1 && j == n){//3-е условие
                j = n - 2;
                i=0;
            }
            else{//1-е условие
                if (j == n)
                    j=0;
                if (i<0)
                    i=n-1;
            }
            if (square[i][j]!=0){ //2-е условие
                j -= 2;
                i++;
                continue;
            }
            else
                square[i][j]=num++;
            j++;//1-е условие
            i--;
        }
        for (int[] array : square) {//вывод, : перебор всех элементов
            for (int x : array) {
                System.out.print(x + "\t");
            }
            System.out.println();
        }
        System.out.println("");
    }

    public static void even(int n){
        int[][] square = new int[n][n];
        int c = 1;
        int count =0;
        for (int i = 0; i < square.length; i++) {//заполнение
            for (int j = 0; j < square.length; j++) {
                square[i][j] = c;
                c++;
            }
        }
        for (int i = 0; i < square.length/2; i++) {//главная
            int temp = square[i][i];
            if(i<square.length/2){
                temp = square[i][i];
                square[i][i] = square[square.length-1-count][square.length-1-count];
                square[square.length-1-count][square.length-1-count] = temp;
                count++;
            }
        }
        count = 0;
        for (int i = 0; i <square.length/2 ; i++) {//побочная
            int temp;
            if(i<square.length/2){
                temp = square[square.length - 1 -count][i];
                square[square.length - 1 - count][i] = square[i][square.length-1-count];
                square[i][square.length-1-count] = temp;
                count++;
            }
        }
        for (int[] array : square) {//вывод, : перебор всех элементов
            for (int x : array) {
                System.out.print(x + "\t");
            }
            System.out.println();
        }
        System.out.println("");
    }
}
