package laba;

import java.util.Random;
import java.util.Scanner;

public class zad39 {
    public static void main(){
        Random rand = new Random();
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()){
            int n = sc.nextInt();
            int m = sc.nextInt();
            int[][] arr = new int[n][m];
            for (int i = 0; i < n; i++){
                for (int j = 0; j < m; j++){
                    arr[i][j] = rand.nextInt(20);
                    System.out.print(arr[i][j] + " ");
                }
                System.out.println();
            }
            int max = Math.abs(arr[0][0]), indexi= 0, indexj=0;
            for (int i = 0; i < n; i++){
                for (int j = 0; j < m; j++){
                    if (max<Math.abs(arr[i][j])){
                        max = Math.abs(arr[i][j]);
                        indexi = i;
                        indexj = j;
                    }
                }
            }
            System.out.println("Введите k: ");
            int k = sc.nextInt();
            int[] rowTemp = new int[n];
            int[] colTemp = new int[m];
            //int[][] temp = new int[n][m];
            for(int i = 0; i < n; i++) {
                colTemp[i] = arr[i][indexj];
                arr[i][indexj] = arr[i][k];
                arr[i][k] = colTemp[i];
            }
            for(int j = 0; j < m; j++) {
                rowTemp[j] = arr[indexi][j];
                arr[indexi][j] = arr[k][j];
                arr[k][j] = rowTemp[j];
            }
            /*for (int i = 0; i < n; i++){
                for (int j = 0; j < m; j++){
                    temp[i][j] = arr[indexi][indexj];
                    arr[indexi][indexj] = arr[k][k];
                    arr[k][k] = temp[i][j];
                }
            }*/
            for (int i[] : arr){
                for (int j : i){
                    System.out.print(j + " ");
                }
                System.out.println();
            }
        }
    }
}
