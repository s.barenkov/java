package laba;

import java.util.Random;
import java.util.Scanner;

public class zad40 {
    public static void main(){
        Random rand = new Random();
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()){
            int n = sc.nextInt();
            int[][] arr = new int[n][n];
            for (int i = 0; i< arr.length;i++){
                for (int j = 0; j < arr[i].length;j++){
                    arr[i][j] = rand.nextInt(21)-10;
                    System.out.print(arr[i][j] + " ");
                }
                System.out.println();
            }
            for (int i = 0; i< arr.length;i++){
                for (int j = 0; j < arr[i].length;j++){
                    if (arr[i][j]<0){
                        arr[i][j] = 0;
                    }
                    else {
                        arr[i][j] = 1;
                    }
                }
            }
            System.out.println();
            for (int i[] : arr){
                for (int j : i){
                    System.out.print(j + " ");
                }
                System.out.println();
            }
        }
    }
}
