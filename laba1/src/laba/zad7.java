/* Написать программу, которая загадывает случайное целое число из отрезка [1;10]
 и просит пользователя его угадать, вводя варианты с клавиатуры,
 пока пользователь не угадает число, программа будет ему подсказывать, сообщая больше
 или меньше число загаданное, чем то, что ввёл пользователь. */
package laba;

import java.util.Scanner;
import java.util.Random;

public class zad7 {
    public static void main(){
        Scanner sc = new Scanner(System.in);
        double rand = (int)(Math.random()*10+1);
        boolean run = true;
        int n;
        System.out.println("Угадайте число от 1 до 10");
        System.out.println("Введите число: ");
        if (sc.hasNextInt()){
            System.out.println("Введите число: ");
            while (run){
                n = sc.nextInt();
                if (rand == n){
                    System.out.println("Поздравляем вы угадали!");
                    run = false;
                }
                else if(rand < n){
                    System.out.println("Ваше число больше");
                }
                else if(rand > n){
                    System.out.println("Ваше число меньше");
                }
            }
        }
        else {
            System.out.println("Вы ввели не целое число или строку");
        }
    }
}
