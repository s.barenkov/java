package laba;

public class Array {
    private String arr;

    public Array(String arr) {
        this.arr = arr;
    }

    public Array() {
    }

    public String getArr() {
        return arr;
    }

    public void setArr(String arr) {
        this.arr = arr;
    }

    @Override
    public String toString() {
        return "Множество: " + arr;
    }

    public static void FindSymbol(Array m, char n) {
        int k = 0;
        for (int i = 0; i < m.getArr().length(); i++) {
            if (m.getArr().charAt(i) == n) {
                System.out.println("Символ " + n + " присутствует в строке.");
                k++;
            }
        }
        if (k == 0) {
            System.out.println("В строке нет такого символа.");
        }
    }

    public static void Crossing(Array m, Array n) {
        int k = 0;
        char ch;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < m.getArr().length(); i++) {
            for (int l = 0; l < n.getArr().length(); l++) {
                if (m.getArr().charAt(i) == n.getArr().charAt(l)) {
                    ch = m.getArr().charAt(i);
                    sb.append(ch);
                    k++;
                }
            }
        }
        if (k == 0) {
            System.out.println("Пересечения в данных множествах отсутствуют.");
        }
        else if (k > 0) {
            System.out.println("Общие символы множеств: ");
            for (int i = 0; i < sb.length(); i++) {
                System.out.print(sb.charAt(i));
            }
        }
        System.out.println();
    }

    public static void Grouping(Array m, Array n) {
        String str1 = m.getArr();
        String str2 = n.getArr();
        m.setArr(str1 + str2);
        n.setArr(str2 + str1);
        System.out.println("Первое множество после объединения: " + m.getArr());
        System.out.println("Второе множество после объединения: " + n.getArr());
    }

    public static void Difference(Array m, Array n) {
        int k = 0;
        int index;
        String str = m.getArr();
        for (int i = 0; i < m.getArr().length(); i++) {
            for (int l = 0; l < n.getArr().length(); l++) {
                if (m.getArr().charAt(i) == n.getArr().charAt(l)) {
                    index = i - k;
                    str = removeCharAt(str, index);
                    k++;
                }
            }
        }
        if (k == 0) {
            System.out.println("Пересечения в данных множествах отсутствуют.");
        }
        else if (k > 0) {
            m.setArr(str);
            System.out.println("Разность двух множеств: " + m.getArr());
        }
    }

    public static void Sum(Array m, Array n) {
        System.out.println("Результат сложения множеств: " + m.getArr() + n.getArr());
    }

    public static void SumSymbol(Array m, char n) {
        m.setArr(m.getArr() + n);
        System.out.println("Множество после добавление символа: " + m.getArr());
    }

    public static void Couple(Array[] m) {
        StringBuilder sb = new StringBuilder();
        int q = 0;
        int z = 0;
        int index;
        for (Array array : m) {
            sb.append(array.getArr());
        }
        for (int i = 0; i < m.length - 1; i++) {
            for (int j = 0; j < m[i].getArr().length(); j++) {
                for (int k = 0; k < m[i + 1].getArr().length(); k++) {
                    if (m[i].getArr().charAt(j) == m[i + 1].getArr().charAt(k)) {
                        index = z - q;
                        sb = new StringBuilder(removeCharAt(sb.toString(), index));
                        q++;
                    }
                }
                z++;
            }
        }
        System.out.println("Сортировка множество попарно: " + sb);
    }
    public static String removeCharAt(String str, int index) {
        return str.substring(0, index) + str.substring(index + 1);
    }
    public static void main(String[] args) {
        Array arr1 = new Array("FhinHetnv");
        Array arr2 = new Array("jsadguiasvjY");
        Array arr3 = new Array("adsgasg3845");
        Array arr4 = new Array("First");
        Array arr5 = new Array("Second");
        Array[] arr = new Array[3];
        arr[0] = new Array("qweh");
        arr[1] = new Array("dsg");
        arr[2] = new Array("sdagb333" + "-" + "hfbv");
        Array.FindSymbol(arr3, 'd');
        Array.Crossing(arr1, arr2);
        Array.Grouping(arr2, arr3);
        Array.Difference(arr1, arr2);
        Array.Sum(arr4, arr5);
        Array.SumSymbol(arr4, 'N');
        Array.Couple(arr);
    }
}
