package laba;

import java.util.Scanner;

public class Book {
    private String author;
    private String title;
    private String date;
    private int pages;

    public Book(){

    }

    public Book(String author, String title, String date, int pages) {
        this.author = author;
        this.title = title;
        this.date = date;
        this.pages = pages;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public void ChangeAuthor(String NewAuthor){
        setAuthor(NewAuthor);
    }

    public  void ChangeAuthor(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter new Author: ");
        String NewAuthor = sc.nextLine();
        setAuthor(NewAuthor);
    }

    public void ChangeTitle(String NewTitle){
        setTitle(NewTitle);
    }

    public void ChangeTitle(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter new Title: ");
        String NewTitle = sc.nextLine();
        setTitle(NewTitle);
    }

    public void ChangeDate(String NewDate){
        setDate(NewDate);
    }

    public void ChangeDate(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter new release date: ");
        String NewDate = sc.nextLine();
        setDate(NewDate);
    }

    public void ChangePages(int NewPages){
        setPages(NewPages);
    }

    public void ChangePages(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter new number of pages: ");
        int NewPages = sc.nextInt();
        setPages(NewPages);
    }

    public void TitleShow(){
        if (getTitle().equals(getTitle())){
            System.out.println(getAuthor());
            System.out.println(getTitle());
            System.out.println(getDate());
            System.out.println(getPages());
        }
    }

    public static void main(String[] args) {
        Book b = new Book("Mr.Author","This is a book","01.06.2005",300);
        b.TitleShow();
        System.out.println("введите автора");
        b.ChangeAuthor();
        System.out.println("введите название");
        b.ChangeTitle();
        System.out.println("введите дату выхода");
        b.ChangeDate();
        System.out.println("введите страницы");
        b.ChangePages();
        b.TitleShow();
        b.ChangeAuthor("Mrs.Author");
        b.ChangeTitle("New book");
        b.ChangeDate("09.09.2001");
        b.ChangePages(500);
        b.TitleShow();
    }
}
