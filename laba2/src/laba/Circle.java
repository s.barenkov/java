package laba;

public class Circle {
    private int r;
    private int x;
    private int y;

    public Circle() {
    }

    public Circle(int r) {
        this.r = r;
    }

    public Circle(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Circle(int r, int x, int y) {
        this.r = r;
        this.x = x;
        this.y = y;
    }

    public int getR() {
        return r;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void Show(){
         System.out.println("Radius: " + getR());
         System.out.println("X: " + getX());
         System.out.println("Y: " + getY());
    }

    public void changeXY(int x, int y){
        this.x += x;
        this.y += y;
        System.out.println("new x: " + getX());
        System.out.println("new y: " + getY());
    }

    public void changeR(int r){
        this.r += r;
        System.out.println("new r: " + getR());;
    }

    public void Area(){
        double area = Math.PI*getR();
        System.out.println("Area: " + area);
    }

    public void Length(){
        double length = 2*Math.PI*getR();
        System.out.println("Circle length: " + length);
    }

    public static void main(String[] args) {
        Circle c2 = new Circle(5);
        Circle c3 = new Circle(10, 7);
        Circle c4 = new Circle(5, 10, 7);
        c4.Show();
        c3.changeXY(2,3);
        c2.changeR(4);
        c2.Area();
        c2.Length();
    }
}

