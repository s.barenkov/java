package laba;

import java.util.Scanner;

public class Equation {
    private int a;
    private int b;
    private int c;

    public Equation() { }

    public Equation(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double Root(Equation ob) {
        double f = Math.pow(2, b) - 4 * a * c;
        if (f > 0) {
            double x1;
            double x2;
            x1 = (-b - Math.sqrt(f)) / (2 * a);
            x2 = (-b + Math.sqrt(f)) / (2 * a);
            System.out.println("Первый корень уравнения: " + x1 + ", второй корень уравнения: " + x2);
            return Math.max(x1, x2);
        }
        else {
            if (f == 0) {
                double x = (double)-b / (2 * a);
                System.out.println("Корень уравнения: " + x);
                return x;
            }
            else {
                System.out.println("Корни отсутствуют");
                return 0;
            }
        }

    }

    public double Extremum(Equation ob) {
        if (a > 0) {
            double minX = (double)-b / (2 * a);
            double minY = a * Math.pow(2, minX) + b * minX + c;
            System.out.println("Координаты минимума уравнения: (" + minX + "," + minY + ")");
            return minX;
        } else {
            if (a < 0) {
                double maxX = (double)-b / (2 * a);
                double maxY = a * Math.pow(2, maxX) + b * maxX + c;
                System.out.println("Координаты максимума уравнения: (" + maxX + "," + maxY + ")");
                return maxX;
            } else {
                System.out.println("У уравнения нет экстремумов");
                return 0;
            }
        }
    }
    public void Gap(Equation ob) {
        if (a > 0) {
            System.out.println("Промежуток убывания: (-~," + ob.Extremum(ob) + "), промежуток возрастания: (" + ob.Extremum(ob) + ",+~)");
        } else {
            if (a < 0) {
                System.out.println("Промежуток возрастания: (-~," + ob.Extremum(ob) + "), промежуток убывания: (" + ob.Extremum(ob) + ", +~)");
            }
        }
    }
    public void create() {
        System.out.println("Введите a");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            a= sc.nextInt();
        }
        System.out.println("Введите b");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            b= sc.nextInt();
        }
        System.out.println("Введите c");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            c= sc.nextInt();
        }
    }
    public static Equation[] createMas(int n) {
        Equation[] mas = new Equation[n];
        for (int i = 0; i < mas.length; i++) {
            mas[i] = new Equation();
            mas[i].create();
        }
        return mas;
    }
    public static void showMas(Equation[] mas) {
        for (Equation equation : mas) {
            equation.show();
        }
        System.out.println();
    }
    public void show() {
        System.out.println(a + "x^2+" + b + "x+" + c + " = 0");
    }

    public static void MasSort(Equation[] mas) {
        double max = mas[0].Root(mas[0]);
        int indexi = 0;
        for (int i = 0; i < mas.length; i++) {
            for (int j = i + 1; j < mas.length; j++) {
                if (mas[i].Root(mas[i]) > mas[j].Root(mas[j])) {
                    max = mas[j].Root(mas[j]);
                    indexi = j;
                }
            }
        }
        System.out.print("Наибольший корень: " + max + " у уравнения: ");
        mas[indexi].show();
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        Equation ob = new Equation(a, b, c);
        ob.Root(ob);
        ob.Extremum(ob);
        ob.Gap(ob);
        System.out.println("введите n");
        int n = sc.nextInt();
        Equation[] mas = Equation.createMas(n);
        Equation.showMas(mas);
        Equation.MasSort(mas);

    }
}
