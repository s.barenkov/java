package laba;

import java.util.Scanner;

public class Fraction {
    private int m;
    private int n;

    public Fraction() {
    }
    public Fraction(int m, int n) {
        this.m = m;
        this.n = n;
    }

    public static int Nod(Fraction fraction1, Fraction fraction2) {
        int first = fraction1.n;
        int second = fraction2.n;
        if (first == 0) {
            return second;
        }
        while (second != 0) {
            if (first > second) {
                first = first - second;
            }
            else {
                second = second - first;
            }
        }
        return first;
    }

    public static int Nod(Fraction fraction1) {
        int first = fraction1.m;
        int second = fraction1.n;
        if (first == 0) {
            return second;
        }
        while (second != 0) {
            if (first > second) {
                first = first - second;
            }
            else {
                second = second - first;
            }
        }
        return first;
    }

    public static int Nok(Fraction fraction1, Fraction fraction2) {
        return (fraction1.n * fraction2.n) / (Fraction.Nod(fraction1, fraction2));
    }

    public static void Sum(Fraction fraction1, Fraction fraction2) {
        int sum = fraction1.m * Fraction.Nok(fraction1, fraction2) / fraction1.n + fraction2.m * Fraction.Nok(fraction1, fraction2) / fraction2.n;
        System.out.println("сумма: " + sum + "/" + Fraction.Nok(fraction1, fraction2));
    }
    public static void Difference(Fraction fraction1, Fraction fraction2) {
        int difference = fraction1.m * Fraction.Nok(fraction1, fraction2) / fraction1.n - fraction2.m * Fraction.Nok(fraction1, fraction2) / fraction2.n;
        System.out.println("разность: " + difference + "/" + Fraction.Nok(fraction1, fraction2));
    }
    public static void Multiplication(Fraction fraction1, Fraction fraction2) {
        int multiplication1 = fraction1.m * fraction2.m;
        int multiplication2 = fraction1.n * fraction2.n;
        System.out.println("произведение: " + multiplication1 + "/" + multiplication2);
    }
    public static void Division(Fraction fraction1, Fraction fraction2) {
        int division1 = fraction1.m * fraction2.n;
        int division2 = fraction1.n * fraction2.m;
        System.out.println("деление: " + division1 + "/" + division2);
    }
    public static void Reduction(Fraction fraction1) {
        if (Fraction.Nod(fraction1) != 1) {
            int first = fraction1.m / Fraction.Nod(fraction1);
            int second = fraction1.n / Fraction.Nod(fraction1);
            System.out.println("сокращение: " + first + "/" + second);
        }
        else {
            System.out.println("дробь несокращаемая");
        }
    }

    public void show() {
        System.out.println(m + "/" + n + ", ");
    }

    public void create() {
        System.out.print("введите числитель: ");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            m = sc.nextInt();
        }
        System.out.print("введите знаменатель: ");
        if (sc.hasNextInt()) {
            n = sc.nextInt();
        }
    }

    public static Fraction[] createMas(int n) {
        Fraction[] mas = new Fraction[n];
        for (int i = 0; i < mas.length; i++) {
            mas[i] = new Fraction();
            mas[i].create();
        }
        return mas;
    }

    public static void showMas(Fraction[] mas) {
        for (Fraction fraction : mas) {
            fraction.show();
        }
        System.out.println();
    }

    public static Fraction[] createArr(int n, Fraction fraction1, Fraction fraction2, Fraction fraction3) {
        Fraction[] arr = new Fraction[n];
        arr[0] = fraction1;
        arr[1] = fraction2;
        arr[2] = fraction3;
        return arr;
    }

    public static void showArr(Fraction[] arr) {
        for (Fraction fraction : arr) {
            fraction.show();
        }
        System.out.println();
    }

    public static void changeArr(Fraction[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (i % 2 == 0) {
                arr[1].m = arr[1].m * Fraction.Nok(arr[1], arr[2]) / arr[1].n + arr[2].m * Fraction.Nok(arr[1], arr[2]) / arr[2].n;
                arr[1].n = Fraction.Nok(arr[1], arr[2]);
            }
            arr[i].show();
        }
    }
    public static void changeMas(Fraction[] mas) {
        for (int i = 0; i < mas.length; i++) {
            if (i % 2 == 0) {
                mas[1].m = mas[1].m * Fraction.Nok(mas[1], mas[2]) / mas[1].n + mas[2].m * Fraction.Nok(mas[1], mas[2]) / mas[2].n;
                mas[1].n = Fraction.Nok(mas[1], mas[2]);
            }
            mas[i].show();
        }
    }
    public static void main(String[] args) {
        Fraction fraction1 = new Fraction(6, 8);
        Fraction fraction2 = new Fraction(3, 4);
        Fraction fraction3 = new Fraction(6, 8);
        Fraction.Sum(fraction1, fraction2);
        Fraction.Difference(fraction1, fraction2);
        Fraction.Multiplication(fraction1, fraction2);
        Fraction.Division(fraction1, fraction2);
        Fraction.Reduction(fraction1);
        Scanner sc = new Scanner(System.in);
        System.out.println("введите k");
        int k = sc.nextInt();
        Fraction[] mas = Fraction.createMas(k);
        Fraction.showMas(mas);
        Fraction[] arr = Fraction.createArr(3, fraction1, fraction2, fraction3);
        Fraction.showArr(arr);
        Fraction.changeArr(arr);
        Fraction.changeMas(mas);
    }
}
