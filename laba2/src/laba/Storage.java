package laba;

import java.util.Scanner;

public class Storage {
    private int quantity;
    private double cost;
    private String goods;
    private int nQuantity;
    private  double nCost;


    public Storage() {
    }

    public Storage(String goods, int quantity, double cost) {
        this.goods = goods;
        this.quantity = quantity;
        this.cost = cost;
    }

    public int getQuantity() {
        return quantity;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public double getCost() {
        return cost;
    }
    public void setCost(double cost) {
        this.cost = cost;
    }
    public String getGoods() {
        return goods;
    }
    public void setGoods(String goods) {
        this.goods = goods;
    }
    public  int getNQuantity(){
        return nQuantity;
    }
    public void setNQuantity(int nQuantity){
        this.nQuantity = nQuantity;
    }
    public double getNCost(){
        return nCost;
    }
    public void setNCost(double nCost) {
        this.nCost = nCost;
    }

    @Override
    public String toString() {
        return "Storage{"+
                "Название= " + goods +
                ", количество= " + quantity +
                ", стоимость= " + cost +
                "}";
    }

    public void create() {
        System.out.println("Введите название предмета");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nGoods = sc.nextLine();
            setGoods(nGoods);
        }
        System.out.println("Введите количество товара");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nQuantity = sc.nextInt();
            setQuantity(nQuantity);
        }
        System.out.println("Введите стоимость товара");
        sc = new Scanner(System.in);
        if (sc.hasNextDouble()) {
            double nCost = sc.nextDouble();
            setCost(nCost);
        }
    }

    public static Storage[] createMas(int n) {
        Storage[] mas = new Storage[n];
        for (int i = 0; i < mas.length; i++) {
            mas[i] = new Storage();
            mas[i].create();
        }
        return mas;
    }
    public static void showMas(Storage[] mas) {
        for (Storage storage : mas) {
            System.out.println(storage);
        }
        System.out.println();
    }

    public static void ChangeQuantity(Storage[] mas, int NewQuantity) {
        for (Storage storage : mas) {
            int q = storage.getQuantity() + NewQuantity;
            storage.setNQuantity(q);
            System.out.println("название:" + storage.getGoods() + ", количество:" + storage.getNQuantity() + ", стоимость:" + storage.getCost());
        }
    }
    public static void ChangeCost(Storage[] mas, double NewCost) {
        for (Storage storage : mas) {
            double c = (storage.getCost() + NewCost);
            storage.setNCost(c);
            System.out.println("название:" + storage.getGoods() + ", количество:" + storage.getNQuantity() + ", стоимость:" + storage.getNCost());
        }
    }
    public static void FullCost(Storage[] mas) {
        for (Storage storage : mas) {
            double fullCost = storage.getNQuantity() * storage.getNCost();
            System.out.println("название: " + storage.getGoods() + ", полная стоимость: " + fullCost);
        }
    }
    public static void CompareGoods(Storage[] mas) {
        double max = mas[0].getNCost();
        for (int i = 1; i < mas.length; i++) {
            if (mas[i].getNCost() > max) {
                max = mas[i].getNCost();
            }
        }
        System.out.println("Максимальная цена: " + max);
    }
    public static void TotalGoods(int...i) {
        int total = 0;
        for (int x : i) {
            total += x;
        }
        System.out.println("Общее количество товара: " + total);
    }
    public static void main ( String[] args ){
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите размер списка");
        if (sc.hasNextInt()) {
            int n = sc.nextInt();
            Storage[] mas = Storage.createMas(n);
            Storage.showMas(mas);
            System.out.println("Измените кол-во товара");
            int NewQuantity = sc.nextInt();
            Storage.ChangeQuantity(mas, NewQuantity);
            System.out.println("Измените стоимость товара");
            double NewCost = sc.nextDouble();
            Storage.ChangeCost(mas, NewCost);
            Storage.FullCost(mas);
            Storage.CompareGoods(mas);
            Storage.TotalGoods(22, 12, 31,10);
        }
    }
}
