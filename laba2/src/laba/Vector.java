package laba;

import java.util.Arrays;
import java.util.Scanner;

public class Vector {
    private int x;
    private int y;
    private int z;

    public Vector() {
    }

    public Vector(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() { return x; }

    public void setX(int x) { this.x = x; }

    public int getY() { return y; }

    public void setY(int y) { this.y = y; }

    public int getZ() { return z; }

    public void setZ(int z) { this.z = z; }

    public void create() {
        System.out.println("Введите x");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nX = sc.nextInt();
            setX(nX);
        }
        System.out.println("Введите y");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nY = sc.nextInt();
            setY(nY);
        }
        System.out.println("Введите z");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nZ = sc.nextInt();
            setZ(nZ);
        }
    }

    public static Vector[] createMas(int n) {
        Vector[] mas = new Vector[n];
        for (int i = 0; i < mas.length; i++) {
            mas[i] = new Vector();
            mas[i].create();
        }
        return mas;
    }
    public static void showMas(Vector[] mas) {
        for (Vector vec : mas) {
            vec.show();
        }
        System.out.println();
    }
    public void show() {
        System.out.println("x=" +x + "y=" + y + "z=" + z);
    }

    public static void Sum(Vector[] mas){
        int sumX = 0, sumY = 0, sumZ = 0;
        for (Vector vec : mas){
            if(vec != null){
                sumX += vec.getX();
                sumY += vec.getY();
                sumZ += vec.getZ();
            }
        }
        System.out.println("Сумма векторов \n" + "x=" + sumX + "\n" + "y=" + sumY + "\n" + "z=" + sumZ);
    }
    public static void Dif(Vector[] mas){
        int difX = mas[0].x, difY = mas[0].y, difZ = mas[0].z;
        for (Vector vec : mas){
            if(vec != null){
                if (vec != mas[0]){
                    difX -= vec.getX();
                    difY -= vec.getY();
                    difZ -= vec.getZ();
                }
            }
        }
        System.out.println("разность векторов \n" + "x=" + difX + "\n" + "y=" + difY + "\n" + "z=" + difZ);
    }
    public static void Multi(Vector[] mas){
        int multiX = 1, multiY = 1, multiZ = 1;
        for (Vector vec : mas){
            if(vec != null){
                multiX *= vec.getX();
                multiY *= vec.getY();
                multiZ *= vec.getZ();
            }
        }
        System.out.println("произведение векторов \n" + "x=" + multiX + "\n" + "y=" + multiY + "\n" + "z=" + multiZ);
    }
    public static void Inc(Vector[] mas){
        int nX, nY, nZ;
        for (int i = 0; i < mas.length; i++) {
            nX= mas[i].x;
            nY= mas[i].y;
            nZ= mas[i].z;
            nX++;
            nY++;
            nZ++;
            System.out.println("Новый инкрементированный вектор" + i+1 + " \n" + "x=" + nX + "\n" + "y=" + nY + "\n" + "z=" + nZ);
        }
    }
    public static void Dec(Vector[] mas){
        int nX, nY, nZ;
        for (int i = 0; i < mas.length; i++) {
            nX= mas[i].x;
            nY= mas[i].y;
            nZ= mas[i].z;
            nX--;
            nY--;
            nZ--;
            System.out.println("Новый декрементированный вектор" + i+1 + " \n" + "x=" + nX + "\n" + "y=" + nY + "\n" + "z=" + nZ);
        }
    }
    public static void Index(Vector[] mas){
        for (int i = 0; i < mas.length; i++) {
            System.out.println("Индекс" + i + "для элемента" + mas[i]);
        }
    }
    public static double Scalar(Vector[] mas) {
        int multiX = 1, multiY = 1, multiZ = 1;
        for (Vector vec : mas){
            if(vec != null){
                multiX *= vec.getX();
                multiY *= vec.getY();
                multiZ *= vec.getZ();
            }
        }
        return multiX+multiY+multiZ;
    }
    public static void Length(Vector[] mas) {
        for (int i = 0; i < mas.length; i++) {
            double len = Math.sqrt(Math.pow(2, mas[i].x) + Math.pow(2, mas[i].y) + Math.pow(2, mas[i].z));
            System.out.println("Длина " + i+1  + " вектора = " + len);
        }
   }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("введите размерность вектора");
        int n = sc.nextInt();
        Vector[] mas = Vector.createMas(n);
        showMas(mas);
        Sum(mas);
        Dif(mas);
        Multi(mas);
        Inc(mas);
        Dec(mas);
        Index(mas);
        Scalar(mas);
        Length(mas);
    }
}
