package laba;


import java.util.Arrays;
import java.util.Scanner;

public class Zad1_Abiturient {
    private int id;
    private String name;
    private String lastName;
    private String patronymic;
    private int phone;
    private String address;
    private int[] marks = new int[5];

    public Zad1_Abiturient() {
    }

    public Zad1_Abiturient(int id, String name, String lastName, String patronymic, int phone, String address, int[] marks) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.phone = phone;
        this.address = address;
        this.marks = marks;
    }
    public Zad1_Abiturient(Zad1_Abiturient ob) {
        this.id = ob.id;
        this.name = ob.name;
        this.lastName = ob.lastName;
        this.patronymic = ob.patronymic;
        this.phone = ob.phone;
        this.address = ob.address;
        this.marks = ob.marks;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getLastName() { return lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getPatronymic() { return patronymic; }

    public void setPatronymic(String patronymic) { this.patronymic = patronymic; }

    public int getPhone() { return phone; }

    public void setPhone(int phone) { this.phone = phone; }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public int[] getMarks() { return marks; }

    public void setMarks(int[] marks) { this.marks = marks; }

    @Override
    public String toString(){
        return "Abiturient{" +
                "id= " + id +
                ", name= '" + name + '\'' +
                ", last name= '" + lastName + '\'' +
                ", patronymic= '" + patronymic + '\'' +
                ", phone= '" + phone + '\'' +
                ", marks= " + Arrays.toString(marks) +
                ", address=' " + address + '\'' +
                '}';
    }
    public void create() {
        System.out.println("введите Id: ");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nId = sc.nextInt();
            setId(nId);
        }
        System.out.println("введите имя: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nName = sc.nextLine();
            setName(nName);
        }
        System.out.println("введите фамилию: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nLastName = sc.nextLine();
            setLastName(nLastName);
        }
        System.out.println("введите отчество: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nPatronymic = sc.nextLine();
            setPatronymic(nPatronymic);
        }
        System.out.println("введите телефон: ");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nPhone = sc.nextInt();
            setPhone(nPhone);
        }
        System.out.println("адресс: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nAddress = sc.nextLine();
            setAddress(nAddress);
        }
        System.out.println("оценки: ");
        for (int i=0;i< marks.length;i++){
            sc = new Scanner(System.in);
            if (sc.hasNextInt()){
                int mark = sc.nextInt();
                marks[i] = mark;
            }
        }
    }
    public static Zad1_Abiturient[] createMas(int n) {
        Zad1_Abiturient[] mas = new Zad1_Abiturient[n];
        for (int i = 0; i < mas.length; i++) {
            mas[i] = new Zad1_Abiturient();
            mas[i].create();
        }
        return mas;
    }
    public static void showMas(Zad1_Abiturient[] mas) {
        for (Zad1_Abiturient arr : mas) {
            System.out.println(arr);
        }
    }
    public int avg(int [] marks){
        int result=0;
        for(int i: marks){
            result+=i;
        }
        return result/marks.length;
    }
}
class University{
    public static void main(String[] args) {
        Zad1_Abiturient[] mas =Zad1_Abiturient.createMas(1);
        Zad1_Abiturient.showMas(mas);
        System.out.println("Bad marks");
        BadMarks(mas);
        System.out.println("Avg marks");
        AvgMarks(mas);
        System.out.println("max avg abiturient");
        MaxAvgOut(mas);

    }
    public static void BadMarks(Zad1_Abiturient[] mas){
        Scanner sc = new Scanner(System.in);
        for(Zad1_Abiturient abiturient : mas){
            if (abiturient != null){
                for (int i : abiturient.getMarks()){
                    if (i<=3){
                        System.out.println(abiturient);
                    }
                }
            }
        }
    }
    public static void AvgMarks(Zad1_Abiturient[] mas){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter avg mark: ");
        int avgMark = sc.nextInt();
        for (Zad1_Abiturient abiturient : mas) {
            if (abiturient != null){
                if (abiturient.avg(abiturient.getMarks())>avgMark){
                    System.out.println(abiturient);
                }
            }
        }
    }
    public static int MaxAvgMarks(Zad1_Abiturient[] mas){
        int max = 0;
        for (Zad1_Abiturient abiturient : mas) {
            if (abiturient != null){
                if (abiturient.avg(abiturient.getMarks())>max){
                    max = abiturient.avg(abiturient.getMarks());
                }
            }
        }
        return max;
    }
    public static void MaxAvgOut(Zad1_Abiturient[] mas){
        for (Zad1_Abiturient abiturient : mas){
            if (abiturient != null){
                if (MaxAvgMarks(mas) == abiturient.avg(abiturient.getMarks())){
                    System.out.println(abiturient);
                }
            }
        }
    }
}

