package laba;

import java.util.Scanner;

public class Zad1_Car {
    private int id;
    private String brand;
    private String model;
    private int releaseDay;
    private String color;
    private int price;
    private int registerNumber;

    public Zad1_Car() {
    }

    public Zad1_Car(int id, String brand, String model, int releaseDay, String color, int price, int registerNumber) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.releaseDay = releaseDay;
        this.color = color;
        this.price = price;
        this.registerNumber = registerNumber;
    }
    public Zad1_Car(Zad1_Car ob) {
        this.id = ob.id;
        this.brand = ob.brand;
        this.model = ob.model;
        this.releaseDay = ob.releaseDay;
        this.color = ob.color;
        this.price = ob.price;
        this.registerNumber = ob.registerNumber;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getBrand() { return brand; }

    public void setBrand(String brand) { this.brand = brand; }

    public String getModel() { return model; }

    public void setModel(String model) { this.model = model; }

    public int getReleaseDay() { return releaseDay; }

    public void setReleaseDay(int releaseDay) { this.releaseDay = releaseDay; }

    public String getColor() { return color; }

    public void setColor(String color) { this.color = color; }

    public int getPrice() { return price; }

    public void setPrice(int price) { this.price = price; }

    public int getRegisterNumber() { return registerNumber; }

    public void setRegisterNumber(int registerNumber) { this.registerNumber = registerNumber; }
    @Override
    public String toString(){
        return "Student{" +
                "id= " + id +
                ", brand= '" + brand + '\'' +
                ", model= '" + model + '\'' +
                ", release day= '" + releaseDay + '\'' +
                ", color= '" + color + '\'' +
                ", price= '" + price + '\'' +
                ", register number= " + registerNumber +
                '}';
    }
    public void create() {
        System.out.println("введите Id: ");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nId = sc.nextInt();
            setId(nId);
        }
        System.out.println("введите марку: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nBrand = sc.nextLine();
            setBrand(nBrand);
        }
        System.out.println("введите модель: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nModel = sc.nextLine();
            setModel(nModel);
        }
        System.out.println("введите год выхода: ");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nReleaseDay = sc.nextInt();
            setReleaseDay(nReleaseDay);
        }
        System.out.println("введите цвет: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nColor = sc.nextLine();
            setColor(nColor);
        }
        System.out.println("цену: ");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nPrice = sc.nextInt();
            setPrice(nPrice);
        }
        System.out.println("регистрационный номер: ");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nRegisterNumber = sc.nextInt();
            setRegisterNumber(nRegisterNumber);
        }
    }
    public static Zad1_Car[] createMas(int n) {
        Zad1_Car[] mas = new Zad1_Car[n];
        for (int i = 0; i < mas.length; i++) {
            mas[i] = new Zad1_Car();
            mas[i].create();
        }
        return mas;
    }
    public static void showMas(Zad1_Car[] mas) {
        for (Zad1_Car arr : mas) {
            System.out.println(arr);
        }
    }
}
class Car{
    public static void main(String[] args) {
        Zad1_Car[] mas =Zad1_Car.createMas(1);
        Zad1_Car.showMas(mas);
        System.out.println("some brand");
        Brand(mas);
        System.out.println("some model and release year");
        ModelAndLifetime(mas);
        System.out.println("some year and more than some price");
        YearAndPrice(mas);
    }
    public static void Brand(Zad1_Car[] mas){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter brand: ");
        String brand = sc.nextLine();
        for(Zad1_Car car : mas){
            if (car != null){
                if (car.getBrand().equals(brand)){
                    System.out.println(car);
                }
            }
        }
    }
    public static void ModelAndLifetime(Zad1_Car[] mas){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter model: ");
        String model = sc.nextLine();
        System.out.println("Enter lifetime: ");
        int lifetime = sc.nextInt();
        for(Zad1_Car car : mas){
            if (car != null){
                if (car.getModel().equals(model)&&lifetime<car.getReleaseDay()){
                    System.out.println(car);
                }
            }
        }
    }
    public static void YearAndPrice(Zad1_Car[] mas){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter year: ");
        int year = sc.nextInt();
        System.out.println("Enter price: ");
        int price = sc.nextInt();
        for(Zad1_Car car : mas){
            if (car != null){
                if (car.getReleaseDay()==year && price<car.getPrice()){
                    System.out.println(car);
                }
            }
        }
    }
}
