package laba;

import java.util.Scanner;

public class Zad1_House {
    private int id;
    private int numberOfFlat;
    private double sq;
    private int floor;
    private int numberOfRooms;
    private String street;
    private String typeOfBuilding;
    private int lifetime;

    public Zad1_House() {
    }

    public Zad1_House(int id, int numberOfFlat, double sq, int floor, int numberOfRooms, String street, String typeOfBuilding, int lifetime) {
        this.id = id;
        this.numberOfFlat = numberOfFlat;
        this.sq = sq;
        this.floor = floor;
        this.numberOfRooms = numberOfRooms;
        this.street = street;
        this.typeOfBuilding = typeOfBuilding;
        this.lifetime = lifetime;
    }
    public Zad1_House(Zad1_House ob) {
        this.id = ob.id;
        this.numberOfFlat = ob.numberOfFlat;
        this.sq = ob.sq;
        this.floor = ob.floor;
        this.numberOfRooms = ob.numberOfRooms;
        this.street = ob.street;
        this.typeOfBuilding = ob.typeOfBuilding;
        this.lifetime = ob.lifetime;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public int getNumberOfFlat() { return numberOfFlat; }

    public void setNumberOfFlat(int numberOfFlat) { this.numberOfFlat = numberOfFlat; }

    public double getSq() { return sq; }

    public void setSq(double sq) { this.sq = sq; }

    public int getFloor() { return floor; }

    public void setFloor(int floor) { this.floor = floor; }

    public int getNumberOfRooms() { return numberOfRooms; }

    public void setNumberOfRooms(int numberOfRooms) { this.numberOfRooms = numberOfRooms; }

    public String getStreet() { return street; }

    public void setStreet(String street) { this.street = street; }

    public String getTypeOfBuilding() { return typeOfBuilding; }

    public void setTypeOfBuilding(String typeOfBuilding) { this.typeOfBuilding = typeOfBuilding; }

    public int getLifetime() { return lifetime; }

    public void setLifetime(int lifetime) { this.lifetime = lifetime; }

    @Override
    public String toString(){
        return "Student{" +
                "id= " + id +
                ", number of flats= '" + numberOfFlat + '\'' +
                ", area= '" + sq + '\'' +
                ", floor= '" + floor + '\'' +
                ", number of rooms= '" + numberOfRooms + '\'' +
                ", street= '" + street + '\'' +
                ", type of buildings= " + typeOfBuilding +
                ", lifetime=' " + lifetime + '\'' +
                '}';
    }
    public void create() {
        System.out.println("введите Id: ");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nId = sc.nextInt();
            setId(nId);
        }
        System.out.println("введите номер квартиры: ");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nNumberOfFlat = sc.nextInt();
            setNumberOfFlat(nNumberOfFlat);
        }
        System.out.println("введите площадь: ");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nSq = sc.nextInt();
            setSq(nSq);
        }
        System.out.println("введите этаж: ");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nFloor = sc.nextInt();
            setFloor(nFloor);
        }
        System.out.println("введите количесто комнат: ");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nNumberOfRooms = sc.nextInt();
            setNumberOfRooms(nNumberOfRooms);
        }
        System.out.println("улицу: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nStreet = sc.nextLine();
            setStreet(nStreet);
        }
        System.out.println("введите тип здания: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nTypeOfBuilding = sc.nextLine();
            setTypeOfBuilding(nTypeOfBuilding);
        }
        System.out.println("введите срок эксплуатации: ");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nLifetime = sc.nextInt();
            setLifetime(nLifetime);
        }
    }
    public static Zad1_House[] createMas(int n) {
        Zad1_House[] mas = new Zad1_House[n];
        for (int i = 0; i < mas.length; i++) {
            mas[i] = new Zad1_House();
            mas[i].create();
        }
        return mas;
    }
    public static void showMas(Zad1_House[] mas) {
        for (Zad1_House arr : mas) {
            System.out.println(arr);
        }
    }
}
class House{
    public static void main(String[] args) {
        Zad1_House[] mas =Zad1_House.createMas(1);
        Zad1_House.showMas(mas);
        System.out.println("Some rooms");
        NumOfRooms(mas);
        System.out.println("Flat with any rooms and floor in range");
        RangeRoom(mas);

    }
    public static void NumOfRooms(Zad1_House[] mas){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number of rooms: ");
        int rooms = sc.nextInt();
        for(Zad1_House house : mas){
            if (house != null){
                if (rooms == house.getNumberOfRooms()){
                    System.out.println(house);
                }
            }
        }
    }
    public static void RangeRoom(Zad1_House[] mas){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number of rooms: ");
        int room = sc.nextInt();
        System.out.println("Enter the floor from");
        int from = sc.nextInt();
        System.out.println("till");
        int till = sc.nextInt();
        for(Zad1_House house : mas){
            if (house != null){
                if (room == house.getNumberOfRooms()&&from>=house.getFloor()&&till<=house.getFloor()){
                    System.out.println(house);
                }
            }
        }
    }
}
