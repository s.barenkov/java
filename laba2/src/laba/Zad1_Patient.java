package laba;

import java.util.Scanner;

public class Zad1_Patient {
    private int id;
    private String name;
    private String lastName;
    private String patronymic;
    private int numberOfCard;
    private int phone;
    private String address;
    private String diagnosis;

    public Zad1_Patient() {
    }

    public Zad1_Patient(int id, String name, String lastName, String patronymic, int numberOfCard, int phone, String address, String diagnosis) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.numberOfCard = numberOfCard;
        this.phone = phone;
        this.address = address;
        this.diagnosis = diagnosis;
    }
    public Zad1_Patient(Zad1_Patient ob) {
        this.id = ob.id;
        this.name = ob.name;
        this.lastName = ob.lastName;
        this.patronymic = ob.patronymic;
        this.numberOfCard = ob.numberOfCard;
        this.phone = ob.phone;
        this.address = ob.address;
        this.diagnosis = ob.diagnosis;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getLastName() { return lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getPatronymic() { return patronymic; }

    public void setPatronymic(String patronymic) { this.patronymic = patronymic; }

    public int getNumberOfCard() { return numberOfCard; }

    public void setNumberOfCard(int numberOfCard) { this.numberOfCard = numberOfCard; }

    public int getPhone() { return phone; }

    public void setPhone(int phone) { this.phone = phone; }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public String getDiagnosis() { return diagnosis; }

    public void setDiagnosis(String diagnosis) { this.diagnosis = diagnosis; }

    @Override
    public String toString(){
        return "Patient{" +
                "id= " + id +
                ", name= '" + name + '\'' +
                ", last name= '" + lastName + '\'' +
                ", patronymic= '" + patronymic + '\'' +
                ", number of card= '" + numberOfCard + '\'' +
                ", phone= '" + phone + '\'' +
                ", diagnosis= " + diagnosis +
                ", address=' " + address + '\'' +
                '}';
    }
    public void create() {
        System.out.println("введите Id: ");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nId = sc.nextInt();
            setId(nId);
        }
        System.out.println("введите имя: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nName = sc.nextLine();
            setName(nName);
        }
        System.out.println("введите фамилию: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nLastName = sc.nextLine();
            setLastName(nLastName);
        }
        System.out.println("введите отчество: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nPatronymic = sc.nextLine();
            setPatronymic(nPatronymic);
        }
        System.out.println("Номер корты: ");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nNumberOfCard = sc.nextInt();
            setNumberOfCard(nNumberOfCard);
        }
        System.out.println("введите телефон: ");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nPhone = sc.nextInt();
            setPhone(nPhone);
        }
        System.out.println("адресс: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nAddress = sc.nextLine();
            setAddress(nAddress);
        }
        System.out.println("диагноз: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nDiagnosis = sc.nextLine();
            setDiagnosis(nDiagnosis);
        }
    }
    public static Zad1_Patient[] createMas(int n) {
        Zad1_Patient[] mas = new Zad1_Patient[n];
        for (int i = 0; i < mas.length; i++) {
            mas[i] = new Zad1_Patient();
            mas[i].create();
        }
        return mas;
    }
    public static void showMas(Zad1_Patient[] mas) {
        for (Zad1_Patient arr : mas) {
            System.out.println(arr);
        }
    }
}

    class Hospital {
        public static void main(String[] args) {
            Zad1_Patient[] mas =Zad1_Patient.createMas(1);
            Zad1_Patient.showMas(mas);
            System.out.println("Sort by diagnosis");
            Diagnosis(mas);
            System.out.println("Sort by Card");
            NumOfCard(mas);
        }
        public static void Diagnosis(Zad1_Patient[] mas){
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter diagnosis: ");
            String diagnosis = sc.nextLine();
            for(Zad1_Patient patient : mas){
                if (patient != null){
                    if (patient.getDiagnosis().equals(diagnosis)){
                        System.out.println(patient);
                    }
                }
            }
        }
        public static void NumOfCard(Zad1_Patient[] mas){
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter number of card from: ");
            int from = sc.nextInt();
            System.out.println("till:");
            int till = sc.nextInt();
            for (Zad1_Patient patient : mas) {
                if (patient != null){
                    if (patient.getNumberOfCard()>=from && patient.getNumberOfCard()<=till){
                        System.out.println(patient);
                    }
                }
            }
        }
    }