package laba;

import java.util.Scanner;

public class Zad1_Student {
    private int id;
    private String name;
    private String lastName;
    private String patronymic;
    private int birthdayDate;
    private int phone;
    private String faculty;
    private int course;
    private int group;

    public Zad1_Student(){

    }

    public Zad1_Student(int id, String name, String lastName, String patronymic, int birthdayDate, int phone, String faculty, int course, int group) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.birthdayDate = birthdayDate;
        this.phone = phone;
        this.faculty = faculty;
        this.course = course;
        this.group = group;
    }

    public Zad1_Student(Zad1_Student ob) {
        this.id = ob.id;
        this.name = ob.name;
        this.lastName = ob.lastName;
        this.patronymic = ob.patronymic;
        this.birthdayDate = ob.birthdayDate;
        this.phone = ob.phone;
        this.faculty = ob.faculty;
        this.course = ob.course;
        this.group = ob.group;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public int getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(int birthdayDate) {
        this.birthdayDate = birthdayDate;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }


    @Override
    public String toString(){
        return "Student{" +
                "id= " + id +
                ", name= '" + name + '\'' +
                ", last name= '" + lastName + '\'' +
                ", patronymic= '" + patronymic + '\'' +
                ", birthday date= '" + birthdayDate + '\'' +
                ", phone= '" + phone + '\'' +
                ", Faculty= " + faculty +
                ", Course=' " + course + '\'' +
                ", Group=' " + group + '\'' +
                '}';
    }
    public void create() {
        System.out.println("введите Id: ");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
             int nId = sc.nextInt();
            setId(nId);
        }
        System.out.println("введите имя: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nName = sc.nextLine();
            setName(nName);
        }
        System.out.println("введите фамилию: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nLastName = sc.nextLine();
            setLastName(nLastName);
        }
        System.out.println("введите отчество: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nPatronymic = sc.nextLine();
            setPatronymic(nPatronymic);
        }
        System.out.println("введите год рождения: ");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nDate = sc.nextInt();
            setBirthdayDate(nDate);
        }
        System.out.println("введите телефон: ");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nPhone = sc.nextInt();
            setPhone(nPhone);
        }
        System.out.println("введите факультет: ");
        sc = new Scanner(System.in);
        if (sc.hasNextLine()) {
            String nFaculty = sc.nextLine();
            setFaculty(nFaculty);
        }
        System.out.println("введите курс: ");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nCourse = sc.nextInt();
            setCourse(nCourse);
        }
        System.out.println("введите группу: ");
        sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int nGroup = sc.nextInt();
            setGroup(nGroup);
        }
    }
    public static Zad1_Student[] createMas(int n) {
        Zad1_Student[] mas = new Zad1_Student[n];
        for (int i = 0; i < mas.length; i++) {
            mas[i] = new Zad1_Student();
            mas[i].create();
        }
        return mas;
    }
    public static void showMas(Zad1_Student[] mas) {
        for (Zad1_Student arr : mas) {
            System.out.println(arr);
        }
    }
}
class out{
    public static void main(String[] args) {
        Zad1_Student[] mas =Zad1_Student.createMas(1);
        Zad1_Student.showMas(mas);
        System.out.println("sort by faculty:");
        FacultyStudents(mas);
        System.out.println("For each faculty and courses:");
        FacultyAndCourseStudents(mas);
        DateFinder(mas);
        GroupOut(mas);
    }

    public static void FacultyStudents(Zad1_Student[] mas){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Faculty: ");
        String fac = sc.nextLine();
        for(Zad1_Student student : mas){
            if (student != null){
                if (student.getFaculty().equals(fac)){
                    System.out.println(student);
                }
            }
        }
    }

    public static void FacultyAndCourseStudents(Zad1_Student[] mas){
        for (Zad1_Student student : mas) {
            if (student != null) {
                if (student.getFaculty().equals("FIK"))
                    System.out.println(student);
            }
        }
        for (Zad1_Student student : mas) {
            if (student != null) {
                if (student.getFaculty().equals("KSIS"))
                    System.out.println(student);
            }
        }


        System.out.println("the first course: ");
        for (Zad1_Student student : mas) {
            if (student != null) {
                if (student.getCourse()==1)
                    System.out.println(student);
            }
        }

        System.out.println("the second course: ");
        for (Zad1_Student student : mas) {
            if (student != null) {
                if (student.getCourse()==2)
                    System.out.println(student);
            }
        }

        System.out.println("the third course: ");
        for (Zad1_Student student : mas) {
            if (student != null) {
                if (student.getCourse()==3)
                    System.out.println(student);
            }
        }

        System.out.println("the fourth course: ");
        for (Zad1_Student student : mas) {
            if (student != null) {
                if (student.getCourse()==4)
                    System.out.println(student);
            }
        }
    }

    public static void DateFinder(Zad1_Student[] mas){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Birthday date: ");
        int DateBirth = sc.nextInt();
        for (Zad1_Student student : mas) {
            if (student != null){
                if (student.getBirthdayDate()>DateBirth){
                    System.out.println(student);
                }
            }
        }
    }

    public static void GroupOut(Zad1_Student[] mas){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Group: ");
        int groupst = sc.nextInt();
        for (Zad1_Student student : mas) {
            if (student != null){
                if (student.getGroup() == groupst){
                    System.out.println(student);
                }
            }
        }
    }
}
