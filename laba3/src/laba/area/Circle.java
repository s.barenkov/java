package laba.area;

import java.util.Scanner;

public class Circle implements Area {
    private final int radius;
    public Circle(int radius) {
        this.radius = radius;
    }
    @Override
    public void area() {
        System.out.println("Площадь круга "+Math.PI*Math.pow(radius, 2));
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter n");
        int n = sc.nextInt();
        Area[] mas = new Area[n];
        for (int i = 0; i < mas.length; i++) {
            System.out.println("enter radius:");
            int r = sc.nextInt();
            mas[i] = new Circle(r);
            System.out.println("radius=" + r);
        }
        for (Area area:mas) {
            area.area();
        }
    }
}

