package laba.area;

import java.util.Scanner;

public class Trapezium implements Area {
    private final int a;
    private final int b;
    private final int h;
    public Trapezium(int a,int b, int h) {
        this.a = a;
        this.b = b;
        this.h = h;
    }

    @Override
    public void area() {
        System.out.println("Площадь трапеции: " + (a+b)*(h/2));
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter n");
        int n = sc.nextInt();
        Area[] mas = new Area[n];
        for (int i = 0; i < mas.length; i++) {
            System.out.println("enter a:");
            int a = sc.nextInt();
            System.out.println("enter b:");
            int b = sc.nextInt();
            System.out.println("enter h:");
            int h = sc.nextInt();
            mas[i] = new Trapezium(a,b,h);
            System.out.println("a=" + a + " b=" + b + "h=" + h);
        }
        for (Area area:mas) {
            area.area();
        }
    }
}
