package laba.arithmetic;

import java.util.Scanner;

public class String1 implements Arithmetic{
    private String[] mas;

    public String1(String[] mas) {
        this.mas = mas;
    }

    public String[] getMas() {
        return mas;
    }

    public void setMas(String[] mas) {
        this.mas = mas;
    }

    @Override
    public void Sum() {
        StringBuffer sb = new StringBuffer();
        for (String ma : mas) {
            sb.append(ma);
        }
        System.out.println("sum=" + sb);
    }

    @Override
    public void Compare() {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter char array: ");
        String compare = sc.nextLine();
        for (int i = 0; i < mas.length; i++) {
            if (compare.equals(mas[i])){
                System.out.println("char array=" + mas[i] + " ,and index=" + i);
            }
            else System.out.println("error");
        }
    }
    public void Filling(){
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < mas.length; i++) {
            System.out.println("enter char array: ");
            mas[i] = sc.nextLine();
        }
    }
    public void show(){
        for (String i : mas ) {
            System.out.print('"' + i + '"' + ", ");
        }
        System.out.println();
    }
    public void Out(){
        Scanner sc = new Scanner(System.in);
        System.out.println("enter index: ");
        int index = sc.nextInt();
        for (int i = 0; i < mas.length; i++) {
            if (index == i){
                System.out.println("element with index " + index + "=" + mas[i]);
            }
            else System.out.println("error");
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter n: ");
        int n = sc.nextInt();
        String[] mas = new String[n];
        String1 arr = new String1(mas);
        arr.Filling();
        arr.show();
        arr.Out();
        arr.Sum();
        arr.Compare();
    }
}
