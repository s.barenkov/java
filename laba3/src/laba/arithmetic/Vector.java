package laba.arithmetic;

import java.util.Scanner;

public class Vector implements Arithmetic{
    int[] vector;
    int[] vector2;
    int[] vector3;

    public Vector(int[] vector, int[] vector2, int[] vector3) {
        this.vector = vector;
        this.vector2 = vector2;
        this.vector3 = vector3;
    }

    @Override
    public void Sum() {
        System.out.print("sum=");
        for (int i = 0; i < vector.length; i++) {
            vector3[i] = vector2[i] + vector[i];
            System.out.print(vector3[i] + ", ");
        }
    }

    @Override
    public void Compare() {
        double l1=0,l2=0;
        for (int i : vector) {
            l1 += Math.sqrt(Math.pow(2, i));
        }
        for (int i : vector2) {
            l2 += Math.sqrt(Math.pow(2, i));
        }
        double max=l1;
        if (max<l2){
            max=l2;
        }
        System.out.println("max length=" + max);
    }
    public void FillingV1(){
        Scanner sc = new Scanner(System.in);
        System.out.println("enter value:");
        for (int i = 0; i< vector.length;i++){
            vector[i] = sc.nextInt();
        }
    }
    public void FillingV2(){
        Scanner sc = new Scanner(System.in);
        System.out.println("enter value:");
        for (int i = 0; i< vector2.length;i++){
            vector2[i] = sc.nextInt();
        }
    }
    public void showV1(){
        for (int i : vector ) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
    public void showV2(){
        for (int i : vector2 ) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter n: ");
        int n = sc.nextInt();
        int[] vec = new int[n];
        int[] vec2 = new int[n];
        int[] vec3 = new int[n];
        Vector vector = new Vector(vec,vec2,vec3);
        vector.FillingV1();
        vector.FillingV2();
        vector.showV1();
        vector.showV2();
        vector.Compare();
        vector.Sum();
    }
}
