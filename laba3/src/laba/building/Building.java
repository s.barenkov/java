package laba.building;

public interface Building {
    String getDoor();
    int getFloor();
    String getType();
}