package laba.building;

public class Theater extends PublicBuilding{
    private boolean Public;

    public Theater(boolean Public, String door, int floor, String type) {
        this.Public = Public;
        this.door = door;
        this.floor = floor;
        this.type = type;
    }
    public boolean getPublic(){
        return true;
    }

    @Override
    public String getDoor() {
        return this.door;
    }

    @Override
    public int getFloor() {
        return this.floor;
    }

    @Override
    public String getType() {
        return this.type;
    }

    public static void main(String[] args) {
        Theater th = new Theater(true,"wooden",23,"opera");
        System.out.println(th.getDoor() + th.getFloor() + th.getPublic() + th.getType());
    }
}
