package laba.employee;

public interface Employee {
    String getName();
    int getYear();
    String getType();
    boolean getIsEmployee();
}
