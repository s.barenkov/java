package laba.employee;

public class Engineer implements Employee {
    public String name;
    public int year;
    public String type;
    boolean IsEmployee;

    public Engineer(String name, int year, String type, boolean isEmployee) {
        this.name = name;
        this.year = year;
        this.type = type;
        IsEmployee = isEmployee;
    }
    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getYear() {
        return year;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public boolean getIsEmployee() {
        return true;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setType(String type) {
        this.type = type;
    }
}

