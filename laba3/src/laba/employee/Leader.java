package laba.employee;

public class Leader extends Engineer {
    String leaderQualities;

    public Leader(String name, int year, String type, boolean isEmployee, String leaderQualities) {
        super(name, year, type, isEmployee);
        this.leaderQualities = leaderQualities;
    }
    public String getLeaderQualities(){
        return leaderQualities;
    }

    public static void main(String[] args) {
        Leader l = new Leader("Name", 1999,"micro", true, "Reliable");
        System.out.println("имя: " + l.getName() + "год: " + l.getYear() + "тип: " + l.getType()
                + "наемный: " + l.getIsEmployee() + "лидерские качества: " + l.getLeaderQualities());
    }
}
