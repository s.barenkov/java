package laba.norma;

import java.util.Scanner;

public class Complex implements Norm{
    private int x;
    private int y;

    public Complex() {
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void norm() {
        double z = Math.sqrt((x*x)+(y*y));
        System.out.println(Math.pow(z,2));
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Complex complex = new Complex();
        System.out.println("enter x: ");
        int nX = sc.nextInt();
        System.out.println("enter y: ");
        int nY = sc.nextInt();
        complex.setX(nX);
        complex.setY(nY);
        complex.norm();
    }
}
