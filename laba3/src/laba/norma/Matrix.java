package laba.norma;

import java.util.Scanner;

public class Matrix implements Norm{
    private int[][] mas = new int[2][2];
    public Matrix() {
    }

    @Override
    public void norm() {
        int max = mas[0][0];
        for (int i = 1; i< mas.length;i++){
            for (int j = 1; j < mas[i].length; j++) {
                if (max<mas[i][j]){
                    max = mas[i][j];
                }
            }
        }
        System.out.println("max value="+max);
    }
    public void show(){
        for ( int[] i : mas ) {
            for ( int j : i ) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }
    public void Filling(){
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i< mas.length;i++){
            for (int j = 0; j < mas[i].length; j++) {
                System.out.println("enter value: ");
                mas[i][j] = sc.nextInt();
            }
        }
    }

    public static void main(String[] args) {
        Matrix arr = new Matrix();
        arr.Filling();
        arr.show();
        arr.norm();
    }
}
