package laba.norma;

import java.util.Scanner;

public class Vector implements Norm{
    int[] vector = new int[10];

    public Vector() {
    }

    @Override
    public void norm() {
        double res = Math.sqrt(Math.abs(sum()));
        System.out.println("res=" + res);
    }
    public int sum(){
        int sum = 0;
        for (int i : vector) {
            sum += i;
        }
        return sum;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Vector vec = new Vector();
        for (int i = 0; i < vec.vector.length; i++) {
            System.out.println("enter value: ");
            vec.vector[i] = sc.nextInt();
        }
        vec.norm();
    }
}
