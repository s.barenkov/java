package laba.ship;

public class AircraftCarrier extends Warship{
    public AircraftCarrier(String name, int year, String type, int Lenght, boolean IsCargo, String weapon) {
        this.name = name;
        this.year = year;
        this.type = type;
        this.Lenght = Lenght;
        this.IsCargo = IsCargo;
        this.weapon = weapon;
    }
    public String getWeapon(){
        return this.weapon;
    }
    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getYear() {
        return this.year;
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public int getLenght() {
        return this.Lenght;
    }

    @Override
    public boolean getIsCargo() {
        return true;
    }

    public static void main(String[] args) {
        AircraftCarrier ac = new AircraftCarrier("Ship",1999,"war",962545,false,"guns");
        System.out.println("имя: " + ac.getName() + "год: " + ac.getYear() + "тип: " + ac.getType() + "длинна: " + ac.getLenght()
        + "торговый: " + ac.getIsCargo() + "оружие: " + ac.getWeapon());
    }
}
