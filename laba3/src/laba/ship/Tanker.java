package laba.ship;

public class Tanker extends CargoShip{
    int numberOfTeam;
    public Tanker(String name, int year, String type, int Lenght, boolean IsCargo, int numberOfTeam)
    {
        super(name,year,type,Lenght,IsCargo);
        this.numberOfTeam = numberOfTeam;
    }
    public int getNumberOfTeam(){
        return numberOfTeam;
    }

    public static void main(String[] args) {
        Tanker t = new Tanker("Ship",1999,"war",962545,true,500);
        System.out.println("имя: " + t.getName() + "год: " + t.getYear() + "тип: " + t.getType() + "длинна: " + t.getLenght()
                + "торговый: " + t.getIsCargo() + "оружие: " + t.getNumberOfTeam());
    }
}
