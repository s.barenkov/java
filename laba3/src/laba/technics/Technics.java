package laba.technics;

public interface Technics {
    String getName();
    String getType();
    boolean getPowerButton();
}
