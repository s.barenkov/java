package laba.technics;

import laba.ship.AircraftCarrier;

public class VideoPlayer extends Player{
    boolean screen;
    public VideoPlayer(String name, String type, boolean powerButton, boolean screen) {
        this.name = name;
        this.type = type;
        this.powerButton = powerButton;
        this.screen = screen;
    }
    public boolean getScreen(){
        return true;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public boolean getPowerButton() {
        return true;
    }

    public static void main(String[] args) {
        VideoPlayer vp = new VideoPlayer("player", "video", true, true);
        System.out.println("имя: " + vp.getName() + "тип: " + vp.getType() + "кнопка питания: " + vp.getPowerButton()
        + "экран: " + vp.getScreen());
    }
}
