package laba.vehicle;

public class Tram extends PublicVehicle{
    boolean barbell;

    public Tram(boolean barbell, String model, String type, int wheels, int length) {
        this.barbell = barbell;
        this.length = length;
        this.model = model;
        this.type = type;
        this.wheels = wheels;
    }
    public boolean getBarbell(){
        return true;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public int getWheels() {
        return wheels;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public int getLength() {
        return length;
    }

    public static void main(String[] args) {
        Tram tr = new Tram(true,"mgr457","long", 12, 10258);
        System.out.println("провода: " + tr.getBarbell() + "тип: " + tr.getType() + "модель: " + tr.getModel()
                + "колеса: " + tr.getWheels() + "длинна: " + tr.getLength());
    }
}