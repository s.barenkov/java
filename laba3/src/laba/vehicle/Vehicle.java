package laba.vehicle;

public interface Vehicle {
    String getModel();
    int getWheels();
    String getType();
    int getLength();
}
