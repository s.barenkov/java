package laba.abiturient;

public class CorrespondStudent extends Student{
    private boolean selfPreparation;

    public CorrespondStudent(boolean selfPreparation, String name, int Course) {
        this.selfPreparation = selfPreparation;
        this.name = name;
        this.Course = Course;
    }
    public boolean getSelfPreparation(){
        return true;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int Course() {
        return this.Course;
    }
}
