package laba.area;

import java.util.Scanner;

public class Rectangle implements Area{
    private final int a;
    private final int b;
    public Rectangle(int a,int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public void area() {
        System.out.println("Площадь прямоугольника: " + a * b);
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter n");
        int n = sc.nextInt();
        Area[] mas = new Area[n];
        for (int i = 0; i < mas.length; i++) {
            System.out.println("enter a:");
            int a = sc.nextInt();
            System.out.println("enter b:");
            int b = sc.nextInt();
            mas[i] = new Rectangle(a,b);
            System.out.println("a=" + a + " b=" + b);
        }
        for (Area area:mas) {
            area.area();
        }
    }
}
