package laba.arithmetic;

import java.util.Random;
import java.util.Scanner;

public class Matrix implements Arithmetic {
    private double[][] mas;

    public Matrix(double[][] mas) {
        this.mas = mas;
    }

    public double[][] getMas() {
        return mas;
    }

    public void setMas(double[][] mas) {
        this.mas = mas;
    }

    @Override
    public void Sum() {
        int sum=0;
        for (int i = 0; i< mas.length;i++){
            for (int j = 0; j < mas[i].length; j++) {
                sum += mas[i][j];
            }
        }
        System.out.println("sum=" + sum);
    }

    @Override
    public void Compare() {
        double max = mas[0][0];
        for (int i = 1; i< mas.length;i++){
            for (int j = 1; j < mas[i].length; j++) {
                if (max<mas[i][j]){
                    max = mas[i][j];
                }
            }
        }
        System.out.println("max value="+max);
    }
    public void show(){
        for ( double[] i : mas ) {
            for ( double j : i ) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }
    public void FillingConsole(){
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i< mas.length;i++){
            for (int j = 0; j < mas[i].length; j++) {
                System.out.println("enter value: ");
                mas[i][j] = sc.nextInt();
            }
        }
    }
    public void FillingRandom(){
        Random rand = new Random();
        for (int i = 0; i< mas.length;i++){
            for (int j = 0; j < mas[i].length; j++) {
                mas[i][j] = rand.nextInt(30)+1;
            }
        }
    }
    public void FindingValue(){
        Scanner sc = new Scanner(System.in);
        System.out.println("enter first index: ");
        int indexI = sc.nextInt();
        System.out.println("enter second index: ");
        int indexJ = sc.nextInt();
        for (int i = 0; i< mas.length;i++){
            if (indexI==i){
                for (int j = 0; j < mas[i].length; j++) {
                    if (indexJ==j){
                        System.out.println(mas[i][j]);
                    }
                }
            }
        }
    }
    public void Scaling(){
        Scanner sc = new Scanner(System.in);
        System.out.println("enter div value: ");
        int value = sc.nextInt();
        for (int i = 0; i< mas.length;i++){
            for (int j = 0; j < mas[i].length; j++) {
                mas[i][j]=mas[i][j]/value;
            }
        }
        for ( double[] i : mas ) {
            for ( double j : i ) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter m: ");
        int m = sc.nextInt();
        System.out.println("enter n: ");
        int n = sc.nextInt();
        double[][] mas = new double[m][n];
        Matrix arr = new Matrix(mas);
        System.out.println("filling by console values");
        arr.FillingConsole();
        arr.show();
        arr.Sum();
        arr.Compare();
        arr.FindingValue();
        arr.Scaling();
        System.out.println("filling by random values");
        arr.FillingRandom();
        arr.show();
        arr.Sum();
        arr.Compare();
        arr.FindingValue();
        arr.Scaling();
    }
}
