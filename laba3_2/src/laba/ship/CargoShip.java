package laba.ship;

public class CargoShip implements Ship {
    public String name;
    public int year;
    public String type;
    public int Lenght;
    boolean IsCargo;

    public CargoShip(String name, int year, String type, int Lenght, boolean IsCargo) {
        this.name = name;
        this.year = year;
        this.type = type;
        this.Lenght = Lenght;
        this.IsCargo = IsCargo;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getYear() {
        return year;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public int getLenght() {
        return Lenght;
    }

    @Override
    public boolean getIsCargo() {
        return true;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setLenght(int Lenght) {
        this.Lenght = Lenght;
    }
}
