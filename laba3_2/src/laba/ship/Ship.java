package laba.ship;

public interface Ship {
    String getName();
    int getYear();
    String getType();
    int getLenght();
    boolean getIsCargo();
}
