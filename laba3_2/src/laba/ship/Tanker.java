package laba.ship;

public class Tanker extends CargoShip {
    int numberOfTeam;
    public Tanker(String name, int year, String type, int Lenght, boolean IsCargo, int numberOfTeam)
    {
        super(name,year,type,Lenght,IsCargo);
        this.numberOfTeam = numberOfTeam;
    }
    public int getNumberOfTeam(){
        return numberOfTeam;
    }
}
