package laba;

import java.util.*;

public class CoupleSum {
    public static List<Integer> list = new ArrayList<>();
    public static void enterList(){
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
    }

    public static void printList(){
        for (Integer i: list){
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public static void sum(){
        while (list.size() != 1){
            ArrayList<Integer> temp = new ArrayList<>(list);
            list.removeAll(list);
            int index;
            if (temp.size()%2 == 0){
                index = temp.size()/2;
            }
            else {
                index = temp.size()/2 + 1;
                temp.set(index, temp.size()-1);
            }
            int j = 0;
            for (int i = 0; i < index; i++) {
                list.add(temp.get(j)+ temp.get(j+1));
                j += 2;
            }
            for (Integer i: list){
                System.out.print(i + " ");
            }
            System.out.println();
        }
    }


    public static void main(String[] args) {
        enterList();
        printList();
        sum();
    }
}
