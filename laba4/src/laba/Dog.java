package laba;

public class Dog {
    private String name;
    private int age;
    private float weight;
    private float height;

    public Dog(String x, int age, float weight) {
        name = x;
        this.age = age;
        this.weight = weight;
    }

    public static void main(String[] args) {
        Dog dog = new Dog("Jopa", 12,10.5f);
        System.out.println(dog.age);
    }

}

