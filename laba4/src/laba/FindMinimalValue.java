package laba;

import java.util.*;

public class FindMinimalValue {
    public static List<Double> list = new LinkedList<>();
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            enterList();
        }catch (InputMismatchException ex){
            System.out.println("Input Mismatch exception");
            throw new InputMismatchException("enter double value in next try)))");
        }
        printList();
        System.out.println("enter which element you want to delete: ");
        double del = sc.nextDouble();
        delete(del);
        printList();
        System.out.println("enter number: ");
        double number = sc.nextDouble();
        System.out.println("close value to "+ number +" number =" + find(number));
    }

    public static void enterList() throws InputMismatchException{
        Scanner sc = new Scanner(System.in);
        System.out.println("enter size: ");
        double n = sc.nextDouble();
        for (int i = 0; i < n; i++) {
            System.out.println("enter value: ");
            double value = sc.nextDouble();
            add(value);
        }
    }

    public static void add(Double number) {
        list.add(number);
    }

    public static void delete(Double number) {
        list.remove(number);
    }

    public static void printList(){
        for (Double i: list){
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public static Double find(Double number) {
        Iterator<Double> iterator = list.iterator();
        Double min = iterator.next();
        try {
            Double elem = iterator.next();
            if (Math.abs(elem - number) < Math.abs(min - number)) {
                min = elem;
            }
        }
        catch (NoSuchElementException ex){
            ex.printStackTrace();
        }
        return min;
    }
}
