package laba;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Sort {
    public static List<Integer> list = new ArrayList<>();
    public static void main(String[] args) {
        enterList();
        printList();
        int i = 0;
        int res = 0;
        while(i < list.size() - res)
        {
            if(list.get(i) < 0)
            {
                list.add(list.get(i));
                list.remove(list.get(i));
                res++;
            }
            else
                i++;
        }
        printList();
    }

    public static void enterList(){
        Random rand = new Random();
        for (int i = 0; i < 20; i++) {
            int value = rand.nextInt(21)-10;
            list.add(value);
        }
    }

    public static void printList(){
        for (Integer i: list){
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
