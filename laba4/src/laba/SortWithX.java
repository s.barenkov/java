package laba;

import java.util.*;

public class SortWithX{
    public static List<Integer> list = new LinkedList<>();

    public static <T extends Comparable<T>> void moveElementTo(List<T> list, T targetElement) {
        for (int i = 0; i < list.size(); i++) {
            int srcIndex = list.indexOf(list.get(i));
            int targetIndex = list.indexOf(targetElement);
            T temp = list.get(i);
            if (srcIndex < 0) {
                throw new IllegalArgumentException("Element: " + temp + " not in the list!");
            }
            if (targetIndex < 0) {
                throw new IllegalArgumentException("Element: " + targetElement + " not in the list!");
            }
            if (temp.compareTo(targetElement) > 0  && srcIndex < targetIndex) {
                list.remove(temp);
                if (srcIndex < targetIndex) {
                    targetIndex -= 1;
                }
                try {
                    list.add(targetIndex + 1, temp);
                }catch (IndexOutOfBoundsException e){
                    System.out.println("Out of Bounds exception");
                }
                i--;
            }
            else if (temp.compareTo(targetElement) > 0 && srcIndex > targetIndex){
                continue;
            }
            else if (srcIndex < targetIndex && targetElement.compareTo(temp) > 0 || temp.equals(targetElement)){
                continue;
            }
            else {
                list.remove(temp);
                if (srcIndex < targetIndex) {
                    targetIndex -= 1;
                }
                try {
                    list.add(targetIndex, temp);
                }catch (IndexOutOfBoundsException e){
                    System.out.println("Out of Bounds exception");
                }
                finally {
                    System.out.println("Out of Bounds finally block");
                }
            }
        }
    }

    public static void main(String[] args) {
        enterList();
        printList();
        Scanner sc = new Scanner(System.in);
        System.out.println("enter: ");
        int x = sc.nextInt();
        try {
            moveElementTo(list, x);
        } catch (Exception ex){
            ex.printStackTrace();
        }
        printList();
    }

    public static void add(int number) {
        list.add(number);
    }

    public static void enterList() throws InputMismatchException{
        Scanner sc = new Scanner(System.in);
        System.out.println("enter n: ");
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            System.out.println("enter number: ");
            int value = sc.nextInt();
            add(value);
        }
    }

    public static void printList(){
        for (Integer i: list){
            System.out.print(i + " ");
        }
        System.out.println();
    }
}

