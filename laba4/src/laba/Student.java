package laba;

import java.util.*;

public class Student implements Comparable<Student>{
    private String name;
    private int course;

    public Student(String name, int course) {
        this.name = name;
        this.course = course;
    }

    public String getName() {
        return name;
    }

    public int getCourse() {
        return course;
    }

    @Override
    public int compareTo(Student anotherStudent) {
        int result = getCourse() - anotherStudent.getCourse();
        if (result != 0){
            return result / Math.abs(result);
        }

        result = getName().compareTo(anotherStudent.getName());
        if (result != 0){
            return result;
        }

        return 0;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("Student [ ");
        sb.append("Name=").append(name);
        sb.append(", Course=").append(course);
        sb.append(" ]");
        return sb.toString();
    }
}

class Arr{
    public static void main(String[] args) {

        // LinkedList realisation

        LinkedList<Student> student = new LinkedList<>();
        student.add(new Student("Denis",1));
        student.add(new Student("Mark",3));
        student.add(new Student("Kirill",2));
        student.add(new Student("Sergey",4));
        student.add(new Student("Dima",1));
        student.add(new Student("Vova",1));
        student.add(new Student("Sasha",2));
        student.add(new Student("Anna",3));
        student.add(new Student("Masha",4));
        student.add(new Student("Katia",3));
        int course = 1;
        printStudents(student, course);
        course = 2;
        printStudents(student, course);
        course = 3;
        printStudents(student, course);
        course = 4;
        printStudents(student, course);

        //TreeSet sort realisation

        TreeSet<Student> sortStudent = new TreeSet<>();
        sortStudent.add(new Student("Denis",1));
        sortStudent.add(new Student("Mark",3));
        sortStudent.add(new Student("Kirill",2));
        sortStudent.add(new Student("Sergey",4));
        sortStudent.add(new Student("Dima",1));
        sortStudent.add(new Student("Vova",1));
        sortStudent.add(new Student("Sasha",2));
        sortStudent.add(new Student("Anna",3));
        sortStudent.add(new Student("Masha",4));
        sortStudent.add(new Student("Katia",3));
        System.out.println("Sorted: ");
         for (Student st: sortStudent){
             System.out.println(st);
         }

         // union and intersect realisation

        LinkedList<Integer> set1 = new LinkedList<>(Arrays.asList(1,2,3,4,5));
        LinkedList<Integer> set2 = new LinkedList<>(Arrays.asList(1,6,7,8,5));

        System.out.println("Union: ");
        System.out.println(new Arr().union(set1, set2));
        System.out.println("Intersect: ");
        System.out.println(new Arr().intersect(set1, set2));

    }

    public static void printStudents(LinkedList<Student> students,int course){
        System.out.println(course + " year students: ");
        Iterator<Student> iter = students.iterator();
        while (iter.hasNext()) {
            Student student = iter.next();
            if (student.getCourse() == course) {
                System.out.println(student.getName());
            }
        }
    }

    public <T> List<T> union(LinkedList<T> set1, LinkedList<T> set2){
        Set<T> set = new HashSet<>();
        set.addAll(set1);
        set.addAll(set2);
        return new LinkedList<>(set);
    }

    public <T> List<T> intersect(LinkedList<T> set1, LinkedList<T> set2){
        List<T> list = new ArrayList<T>();

        for (T t : set1) {
            if(set2.contains(t)) {
                list.add(t);
            }
        }

        return list;
    }
}
